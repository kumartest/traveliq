<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

if(!function_exists('is_logged_in'))
{
	function is_logged_in()
	{
		// Get current CodeIgniter instance
		$CI =& get_instance();
		// We need to use $CI->session instead of $this->session
		$user = $CI->session->userdata('t_loggedin');
		if ($CI->session->userdata('t_loggedin')) 
		{ 
			return true;
		} 
		else 
		{ 
			redirect('/');
		}
	}
}

if(!function_exists('getModetypes'))
{
	function getModetypes($mode_types)
	{
		$res = "";
		$tmodename = array();
		$mtypes = explode(",",$mode_types);
		$CI =& get_instance();
		$CI->db->select('mode_type');
		$CI->db->from('tbl_travel_modes');
		$CI->db->where_in('id',$mtypes);
		$query = $CI->db->get();
		if($query->num_rows() > 0 ){
			foreach ($query->result_array() as $mres) {
				array_push($tmodename, $mres['mode_type']);
			}
			$res = implode(",", $tmodename);
			return $res;
		}else{
			return $res;
		}
	}
}


?>
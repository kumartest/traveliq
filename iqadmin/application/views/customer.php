<script type="text/javascript">
	$(document).ready(function() {
	    $('#bookings').DataTable( {
	        dom: 'Bfrtip',
	        buttons: [
	            'copy', 'csv', 'excel', 'pdf', 'print'
	        ]
	    } );
	} );
</script>
<section id="main-content">
	<section class="wrapper">
		<div class="table-agile-info rem-pad">
 			<div class="panel panel-default">
 				<?php
				    if($this->session->flashdata('success_msg'))    
				        {           
				    echo "<div class='alert alert-success'>".$this->session->flashdata('success_msg')."</div>"; 
				    }   
				    if($this->session->flashdata('error_msg'))
				        {       
				    echo "<div class='alert alert-danger'>".$this->session->flashdata('error_msg')."</div>";    
				    }           
				?>
 				<div class="panel-heading">
			     	Customer Details
			    </div>
			    <table id="bookings" class="table table-striped table-bordered display nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>Sno</th>
							<th>Customer Name</th>
							<th>Mobile Number</th>
							<th>Email</th>
							<th>Address</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						<?php if($customers->row_array() > 0){
							$i=1;
							foreach ($customers->result_array() as $cust) { ?>
							<tr>
								<td><span class="btn btn-xs btn-primary"><?php echo $i;?></span></td>
								<td><?php echo $cust['customer_name'];?></td>
								<td><?php echo $cust['mobile_number'];?></td>
								<td><?php echo $cust['email'];?></td>
								<td><?php echo $cust['address'];?></td>
								<td><a href="<?=base_url()?>customer/editcustomer/<?php echo $cust['id'];?>" class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i></a>&nbsp;<a href="<?=base_url()?>customer/shwotransactions/<?php echo $cust['id'];?>" class="btn btn-xs btn-primary"><i class="fa fa-eye"></i> Transactions</a></td>
							</tr>
						<?php $i++;	}
						} 
						?>
					</tbody>
				</table>
			</div>
		</div>
	</section>
</section>
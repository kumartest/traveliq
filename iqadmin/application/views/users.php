<script type="text/javascript">
	$(document).ready(function() {
	    $('#example').DataTable( {
	        dom: 'Bfrtip',
	        buttons: [
	            'copy', 'csv', 'excel', 'pdf', 'print'
	        ]
	    } );
	} );
</script>
<section id="main-content">
	<section class="wrapper">
		<div class="table-agile-info rem-pad">
 			<div class="panel panel-default">
 				<?php
				    if($this->session->flashdata('success_msg'))    
				        {           
				    echo "<div class='alert alert-success'>".$this->session->flashdata('success_msg')."</div>"; 
				    }   
				    if($this->session->flashdata('error_msg'))
				        {       
				    echo "<div class='alert alert-danger'>".$this->session->flashdata('error_msg')."</div>";    
				    }           
				?>
 				<div class="panel-heading">
			     	User Details
			     	<a href="<?=base_url()?>users/createUser" class="btn btn-info btn-xs">Create User</a>
			    </div>
				<table id="example" class="table table-striped table-bordered display nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>Sno</th>
							<th>User Name</th>
							<th>Email</th>
							<th>Mobile Number</th>
							<th>Address</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php if($users->num_rows() > 0 ){
							$i = 1;
							foreach ($users->result_array() as $user) { ?>
								<tr>
									<td><?php echo $i; ?></td>
									<td><?php echo $user['user_name']; ?></td>
									<td><?php echo $user['email_id']; ?></td>
									<td><?php echo $user['mobile_number']; ?></td>
									<td><?php echo $user['address']; ?></td>
									<td><a href="<?=base_url()?>users/editUser/<?php echo $user['id'];?>" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>&nbsp;
									<?php if($user['status'] == 1){ ?>
										<a href="<?=base_url()?>users/changestatus/<?php echo $user['id'];?>/0" class="btn btn-success btn-xs changestatus" onClick="changeInActive();"><i class="fa fa-check-circle"></i></a></td>
									<?php }else{ ?>
										<a href="<?=base_url()?>users/changestatus/<?php echo $user['id'];?>/1" class="btn btn-danger btn-xs changestatus" onClick="changeInActive();"><i class="fa fa-times-circle"></i></a></td>
									<?php }?>
								</tr>	
						<?php $i++;	}
						}?>
					</tbody>
				</table>
			</div>
		</div>
	</section>
</section>
<script type="text/javascript">
	function changeInActive()
	{
		var x = confirm("Are you sure you want to Chage Status?");
	    if (x)
	      return true;
	    else
	      return false;
	}
</script>
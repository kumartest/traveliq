<section id="main-content">
	<section class="wrapper">
            <div class="row">
                <div class="col-lg-12 butn">
                	<!-- <div id="formerrors"></div> -->
                	<?php echo validation_errors();?>
                	<?php
                        if($this->session->flashdata('success_msg'))    
                            {           
                        echo "<div class='alert alert-success'>".$this->session->flashdata('success_msg')."</div>"; 
                        }   
                        if($this->session->flashdata('error_msg'))
                            {       
                        echo "<div class='alert alert-danger'>".$this->session->flashdata('error_msg')."</div>";    
                        }           
                    ?>
                	<section >
                        <header class="panel-heading">
                            Create Journey Type </header>
                        <div class="panel-body">
                        	<form role="form" action="<?=base_url()?>journeytype/insertjtype" method="post" name="fctype" id="fctype">
                                <div class="form-group col-sm-6">
                                    <label for="jtypename">Journey Type Name</label> <input type="text" class="ggg" name="jtypename" id="jtypename" placeholder="Enter User Name">
                                </div>
                                <div class="form-group col-sm-6" style="margin-top: 1px;">
                                    <label for="tmodetype">Travel Mode Type</label>
                                    <select class="ggg" name="tmodetype" id="tmodetype">
                                        <option value="">Select</option>
                                        <?php if($tmodes->num_rows() > 0){
                                            foreach ($tmodes->result_array() as $mode) { ?>
                                                <option value="<?php echo $mode['id'];?>"><?php echo $mode['mode_type'];?></option>
                                            <?php }
                                        } ?>
                                    </select>
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="status">Status</label>
                                    <select class="ggg" name="status" id="status">
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                </div>
                                <div class="form-group col-sm-12">
                                	<input type="submit" name="createctype" id="createctype" value="Create">
                                </div>
                            </form>
                        </div>
                    </section>
                </div>
            </div>

    </section>
</section>
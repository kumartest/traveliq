<section id="main-content">
	<section class="wrapper">
            <div class="row">
                <div class="col-lg-12 butn">
                	<!-- <div id="formerrors"></div> -->
                	<?php echo validation_errors();?>
                	<?php
                        if($this->session->flashdata('success_msg'))    
                            {           
                        echo "<div class='alert alert-success'>".$this->session->flashdata('success_msg')."</div>"; 
                        }   
                        if($this->session->flashdata('error_msg'))
                            {       
                        echo "<div class='alert alert-danger'>".$this->session->flashdata('error_msg')."</div>";    
                        }           
                    ?>
                	<section >
                        <header class="panel-heading">
                            Create Journey Type </header>
                        <div class="panel-body">
                        	<form role="form" action="<?=base_url()?>travelmodes/inserttmode" method="post" name="fctype" id="fctype">
                                <div class="form-group col-sm-6">
                                    <label for="travelmode">Travel Mode Name</label> <input type="text" class="ggg" name="travelmode" id="travelmode" placeholder="Enter User Name">
                                </div>
                                <div class="form-group col-sm-6" style="margin-top: 1px;">
                                    <label for="tmodealias">Alias Name</label>
                                    <input type="text" class="ggg" name="tmodealias" id="tmodealias" placeholder="Enter Alias Name">
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="status">Status</label>
                                    <select class="ggg" name="status" id="status">
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                </div>
                                <div class="form-group col-sm-12">
                                	<input type="submit" name="createctype" id="createctype" value="Create">
                                </div>
                            </form>
                        </div>
                    </section>
                </div>
            </div>

    </section>
</section>
<script type="text/javascript">
	$(document).ready(function() {
	    $('#classtypes').DataTable( {
	        dom: 'Bfrtip',
	        buttons: [
	            'copy', 'csv', 'excel', 'pdf', 'print'
	        ]
	    } );
	} );
</script>
<section id="main-content">
	<section class="wrapper">
		<div class="table-agile-info rem-pad">
 			<div class="panel panel-default">
 				<?php
				    if($this->session->flashdata('success_msg'))    
				        {           
				    echo "<div class='alert alert-success'>".$this->session->flashdata('success_msg')."</div>"; 
				    }   
				    if($this->session->flashdata('error_msg'))
				        {       
				    echo "<div class='alert alert-danger'>".$this->session->flashdata('error_msg')."</div>";    
				    }           
				?>
 				<div class="panel-heading">
			     	Class Type Details <a href="<?=base_url()?>classtype/createclasstype" class="btn btn-xs btn-info">Create</a>
			    </div>
			    <table id="classtypes" class="table table-striped table-bordered display nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>Sno</th>
							<th>Type Name</th>
							<th>Trvel Mode Type</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						<?php if($ctypes->num_rows() > 0 ){
							$i=1;
							foreach ($ctypes->result_array() as $cres) { ?>
								<tr>
									<td><?php echo $i;?></td>
									<td><?php echo $cres['type_name'];?></td>
									<td><?php echo $cres['mode_type'];?></td>
									<td><a href="<?=base_url()?>classtype/editclasstype/<?php echo $cres['id'];?>" class="btn btn-xs btn-primary" title="Edit"><i class="fa fa-pencil" ></i></a>&nbsp;<a href="<?=base_url()?>classtype/deleteclasstype/<?php echo $cres['id'];?>" class="btn btn-xs btn-danger" title="Delete"><i class="fa fa-times-circle"></i></a></td>
								</tr>
							<?php $i++; }
						}?>
					</tbody>
				</table>
			</div>
		</div>
	</section>
</section>
<section id="main-content">
	<section class="wrapper">
    	<!-- <div class="form-w3layouts"> -->
            <div class="row">
                <div class="col-lg-12 butn">
                    <div id="formerrors"></div>
                    <?php echo validation_errors();?>
                    <?php
                        if($this->session->flashdata('success_msg'))    
                            {           
                        echo "<div class='alert alert-success'>".$this->session->flashdata('success_msg')."</div>"; 
                        }   
                        if($this->session->flashdata('error_msg'))
                            {       
                        echo "<div class='alert alert-danger'>".$this->session->flashdata('error_msg')."</div>";    
                        }           
                    ?>
                    <section>
                        <header class="panel-heading">
                            Update Transaction
                        </header>
                        <div class="panel-body">
                            <form role="form" action="<?=base_url()?>customer/updatetnx/<?php echo $tdata['id'];?>" method="post" name="udpatetnx" id="udpatetnx">
                                <div class="form-group col-sm-6" style="margin-top: 1px;">
                                    <label for="payment_type" >Payment Type</label>
                                    <select class="ggg" name="payment_type" id="payment_type">
                                        <option value="0" <?php if($tdata['pament_mode'] == 0 ) echo "selected"; ?>>Cash</option>
                                        <option value="1" <?php if($tdata['pament_mode'] == 1 ) echo "selected"; ?>>Cheque</option>
                                        <option value="2" <?php if($tdata['pament_mode'] == 2 ) echo "selected"; ?>>Net Banking</option>
                                    </select>
                                    <input type="hidden" name="ptype" id="ptype" value="<?php echo $tdata['pament_mode'];?>">
                                </div>
                                <div class="form-group col-sm-6" id="pay_cheque" style="display: none;">
                                    <label for="cheque_number">Cheque Number</label>
                                    <input type="text" class="ggg" name="cheque_number" id="cheque_number" placeholder="Enter Cheque Number" value="<?php echo$tdata['reference_number']; ?>">
                                </div>
                                <div class="form-group col-sm-6" id="bank_ref_num" style="display: none;">
                                    <label for="bank_ref_num">Bank Reference Number</label>
                                    <input type="text" class="ggg" name="bank_ref_num" id="bank_ref_num" placeholder="Enter Bank Reference Number" value="<?php echo$tdata['reference_number']; ?>">
                                </div>
                                <div class="form-group col-sm-6" id="bank_ref_num">
                                    <label for="amount">Amount</label>
                                    <input type="text" class="ggg" name="amount" id="amount" placeholder="Enter Amount" value="<?php echo $tdata['amount'];?>" disabled>
                                </div>
                                <div class="form-group col-sm-6" id="bank_ref_num">
                                    <label for="payment_date">Payment Date</label>
                                    <input type="text" class="ggg datepicker" name="payment_date" id="payment_date" data-provide="datepicker" data-date-format="yyyy-mm-dd" placeholder="YYYY-MM-DD" value="<?php echo $tdata['pay_date'];?>">
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="amt_status">Amount Status</label>
                                    <select class="ggg" name="amt_status" id="amt_status">
                                        <option value="0" <?php if($tdata['amount_status'] == 0 ) echo "selected"; ?>>Paid</option>
                                        <option value="1" <?php if($tdata['amount_status'] == 1 ) echo "selected"; ?>>Pending</option>
                                        <option value="2" <?php if($tdata['amount_status'] == 2 ) echo "selected"; ?>>Rejected</option>
                                    </select>
                                </div>
                                <div class="col-sm-12">
                                    <input type="submit" name="regform" class="btn btn-info" value="Update">
                                </div>
                            </form>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </section>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.datepicker').datepicker({
                format: 'yyyy-mm-dd'
            });

            var x = $('#payment_type').val();
            if(x == 1){
                $("#pay_cheque").show();
            }else if(x == 2){
                $("#bank_ref_num").show();
            }else{
                $("#pay_cheque").hide();
                $("#bank_ref_num").hide(); 
            }
             $("#payment_type").change(function()
                {
                    if($(this).val() == "0")
                    {
                        $("#pay_cheque").hide();
                        $("#bank_ref_num").hide();
                    }
                    else if($(this).val() == "1")
                    {
                        $("#bank_ref_num").hide();
                        $("#pay_cheque").show();
                    }else{
                        $("#pay_cheque").hide();
                        $("#bank_ref_num").show();
                    }
                });
        });
    </script>
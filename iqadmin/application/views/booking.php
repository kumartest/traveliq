<script type="text/javascript">
	$(document).ready(function() {
	    $('#bookings').DataTable( {
	        dom: 'Bfrtip',
	        buttons: [
	            'copy', 'csv', 'excel', 'pdf', 'print'
	        ]
	    } );
	} );
</script>
<section id="main-content">
	<section class="wrapper">
		<div class="table-agile-info rem-pad">
 			<div class="panel panel-default">
 				<?php
				    if($this->session->flashdata('success_msg'))    
				        {           
				    echo "<div class='alert alert-success'>".$this->session->flashdata('success_msg')."</div>"; 
				    }   
				    if($this->session->flashdata('error_msg'))
				        {       
				    echo "<div class='alert alert-danger'>".$this->session->flashdata('error_msg')."</div>";    
				    }           
				?>
 				<div class="panel-heading">
			     	Booking Details <!-- <a href="<?=base_url()?>classtype/createclasstype" class="btn btn-xs btn-info">Create</a> -->
			    </div>
			    <table id="bookings" class="table table-striped table-bordered display nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>Sno</th>
							<th>Type Name</th>
							<th>Trvel Mode Type</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						
					</tbody>
				</table>
			</div>
		</div>
	</section>
</section>
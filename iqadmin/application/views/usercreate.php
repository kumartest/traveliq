<section id="main-content">
	<section class="wrapper">
            <div class="row">
                <div class="col-lg-12 butn">
                	<!-- <div id="formerrors"></div> -->
                	<?php echo validation_errors();?>
                	<?php
                        if($this->session->flashdata('success_msg'))    
                            {           
                        echo "<div class='alert alert-success'>".$this->session->flashdata('success_msg')."</div>"; 
                        }   
                        if($this->session->flashdata('error_msg'))
                            {       
                        echo "<div class='alert alert-danger'>".$this->session->flashdata('error_msg')."</div>";    
                        }           
                    ?>
                	<section >
                        <header class="panel-heading">
                            Create User
                        </header>
                        <div class="panel-body">
                        	<form role="form" action="<?=base_url()?>users/insertUser" method="post" name="bookingform" id="bookingform">
                                <div class="form-group col-sm-6">
                                    <label for="username">User Name</label>
                                    <input type="text" class="ggg" name="username" id="username" placeholder="Enter User Name">
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="email">Email Address</label>
                                    <input type="email" class="ggg" name="email" id="email" placeholder="Enter Email Address">
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="user_pwd">Password</label>
                                    <input type="password" class="ggg" name="user_pwd" id="user_pwd" placeholder="Enter Password">
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="mobilenumber">Mobile Number</label>
                                    <input type="text" class="ggg" name="mobilenumber" id="mobilenumber" placeholder="Enter Mobile Number">
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="mobilenumber">Status</label>
                                    <select class="ggg" name="status" id="status">
                                    	<option value="1">Active</option>
                                    	<option value="0">Inactive</option>
                                    </select>
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="address">Address</label>
                                    <textarea class="ggg" name="address" id="address" placeholder="Enter Address"></textarea>
                                </div>
                                <div class="form-group col-sm-12">
                                	<input type="submit" name="createuser" id="createuser" value="Create User">
                                </div>
                            </form>
                        </div>
                    </section>
                </div>
            </div>

    </section>
</section>
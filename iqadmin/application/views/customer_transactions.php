<section id="main-content">
	<section class="wrapper">
		<div class="row">
            <div class="col-lg-12 butn">
            	<div class="col-lg-12">
	                <table class="table table-responsive">
	                	<thead>
	                		<tr>
	                			<th>Sno</th>
	                			<th>Customer Name</th>
	                			<th>Date of booking</th>
	                			<th>Mode type</th>
	                			<th>Payment Type</th>
	                			<th>Reference Number</th>
	                			<th>Status</th>
	                			<th>Amount</th>
	                			<th>Actions</th>
	                		</tr>
	                	</thead>
	                	<tbody id="viewdata">
	                	<?php	if(count($result) > 0){
								for ($i=0; $i < count($result) ; $i++) {  ?>
									<tr>
										<td><?php echo $i+1;?></td>
										<td><?php echo $result[$i]['customer_name']?></td>
										<td><?php echo date('d M,Y',strtotime($result[$i]['dob']));?></td>
										<td><?php echo getModetypes($result[$i]['mode_type']);?></td>
										<td><?php 
											if($result[$i]['pament_mode'] == 0){
												echo '<span class="label label-info">Cash</span>';
											}else if($result[$i]['pament_mode'] == 1){
												echo '<span class="label label-info">Cheque</span>';
											}else{
												echo '<span class="label label-info">Net Banking</span>';
											} ?></td>
										<td><?php if(!empty($result[$i]['reference_number'])){
													echo '<span class="label label-warning">'.$result[$i]['reference_number'].'</span>';
												}else{
													echo '<span class="label label-warning"> N/A </span>';
												} ?>		
										</td>
										<td><?php if($result[$i]['amount_status'] == 0) echo '<span class="label label-success">Paid</span>'; else echo '<span class="label label-danger">Pending</span>'; ?></td>
										<td><?php echo $result[$i]['amount']?></td>
										
										<td><div id="test">
												<?php if($result[$i]['amount_status'] == 1){ 
													?> <a href="<?=base_url()?>customer/updatestatus/<?php echo $result[$i]['tid']?>/0" class="btn btn-xs btn-primary"  name="accpect" id="accpect" onClick="changeInActive();">Accpect</a>&nbsp;
													<a href="<?=base_url()?>customer/updatestatus/<?php echo $result[$i]['tid']?>/2" class="btn btn-xs btn-danger" name="reject" id="reject" onClick="changeInActive();">Reject</a> <?php 
												 } else if($result[$i]['amount_status'] == 2){
													echo '<span class="label label-danger">Rejected</span>';
													?> <a href="<?=base_url()?>customer/edittransaction/<?php echo $result[$i]['tid']?>" class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i></a> <?php 
												} else{
													echo '<span class="label label-success">Paid Successfully.</span>';
												} ?>
											</div>
											<div id="div_accpect" style="display: none;"><?php echo '<span class="label label-success">Accpected</span>'; ?></div>
											<div id="div_reject" style="display: none;"><?php echo '<span class="label label-danger">Rejected</span>'; ?></div>
										</td>
									</tr>
								<?php } ?>
									<tr style="background: #DCD;">
										<td><strong>Paid:</strong></td>
										<td><span class="label label-success"><strong><?php echo $amounts['paid'].".Rs";?></strong></span></td>
										<td></td>
										<td><strong>Balance :</strong></td>
										<td><span class="label label-warning"><strong><?php echo $amounts['pending'].".Rs";?></strong></span></td>
										<td></td>
										<td><strong>Total:</strong></td>
										<td><span class="label label-info"><strong><?php echo $amounts['tot_amount'].".Rs";?></strong></span></td>
										<td></td>
									</tr>
							<?php } else{ ?>
								<tr>
									<td colspan="9">No Results Found....</td>
								</tr>
							<?php }?>
	                	</tbody>
	                </table>
                </div>
            </div>
        </div>
    </section>
</section>

<script type="text/javascript">
	function changeInActive()
	{
		var x = confirm("Are you sure you want to Chage Status?");
	    if (x)
	      return true;
	    else
	      return false;
	}
</script>
sidebar start-->
<aside>
    <div id="sidebar" class="nav-collapse">
        <!-- sidebar menu start-->
        <div class="leftside-navigation">
            <ul class="sidebar-menu" id="nav-accordion">
                

            <?php 
                $selmethods1 = array("dashboard");
                if(in_array($this->router->fetch_method(),$selmethods1)){
                    $clsactive1 = "active";
                }else if(in_array($this->router->fetch_class(),$selmethods1)){
                    $clsactive1 = "active";
                }else{
                    $clsactive1 = "";
                }
            ?>
                <li>
                    <a class="<?php echo $clsactive1;?>" href="<?=base_url();?>dashboard">
                        <i class="fa fa-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
            <?php if($this->session->userdata('role') == 1){ ?>
            <?php 
                $selmethods1 = array("users");
                if(in_array($this->router->fetch_method(),$selmethods1)){
                    $clsactive2 = "active";
                }else if(in_array($this->router->fetch_class(),$selmethods1)){
                    $clsactive2 = "active";
                }else{
                    $clsactive2 = "";
                }
            ?>
                <li>
                    <a class="<?php echo $clsactive2;?>" href="<?=base_url();?>users">
                        <i class="fa fa-users"></i>
                        <span>Users</span>
                    </a>
                </li>
            <!-- Booking History -->
            <?php 
                $selmethods1 = array("bookingactions");
                if(in_array($this->router->fetch_method(),$selmethods1)){
                    $clsactive4 = "active";
                }else if(in_array($this->router->fetch_class(),$selmethods1)){
                    $clsactive4 = "active";
                }else{
                    $clsactive4 = "";
                }
            ?>
                <li>
                    <a class="<?php echo $clsactive4; ?>" href="<?=base_url();?>bookingactions">
                        <i class="fa fa-book"></i>
                        <span>Booking Hisory</span>
                    </a>
                </li>
            <!-- End Booking History -->

            <?php } ?>

            <!-- Customers -->
            <?php 
                $selmethods1 = array("customer");
                if(in_array($this->router->fetch_method(),$selmethods1)){
                    $clsactive5 = "active";
                }else if(in_array($this->router->fetch_class(),$selmethods1)){
                    $clsactive5 = "active";
                }else{
                    $clsactive5 = "";
                }
            ?>
                <li>
                    <a class="<?php echo $clsactive5; ?>" href="<?=base_url();?>customer">
                        <i class="fa fa-users"></i>
                        <span>Customers</span>
                    </a>
                </li>
            <!-- End Customers -->

            <?php if($this->session->userdata('role') == 1){ ?>

            <?php 
                $selmethods1 = array("classtype","journeytype","travelmodes");
                if(in_array($this->router->fetch_method(),$selmethods1)){
                    $clsactive3 = "active";
                }else if(in_array($this->router->fetch_class(),$selmethods1)){
                    $clsactive3 = "active";
                }else{
                    $clsactive3 = "";
                }
            ?>
                <li class="sub-menu">
                    <a class="<?php echo $clsactive3;?>" href="javascript:;">
                        <i class="fa fa-th"></i>
                        <span>Master Data</span>
                    </a>
                    <ul class="sub">
                        <li><a <?php if($this->router->fetch_class() == "classtype") echo 'class="active"';?> href="<?=base_url();?>classtype">Class Type</a></li>
                        <li><a <?php if($this->router->fetch_class() == "journeytype") echo 'class="active"';?> href="<?=base_url();?>journeytype">Journey Type</a></li>
                        <li><a <?php if($this->router->fetch_class() == "travelmodes") echo 'class="active"';?> href="<?=base_url();?>travelmodes">Travel Modes</a></li>
                    </ul>
                </li>
            <?php } ?>
            </ul>            
        </div>
        <!-- sidebar menu end-->
    </div>
</aside>
<!--sidebar end
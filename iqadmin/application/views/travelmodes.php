<script type="text/javascript">
	$(document).ready(function() {
	    $('#travelmode').DataTable( {
	        dom: 'Bfrtip',
	        buttons: [
	            'copy', 'csv', 'excel', 'pdf', 'print'
	        ]
	    } );
	} );
</script>
<section id="main-content">
	<section class="wrapper">
		<div class="table-agile-info rem-pad">
 			<div class="panel panel-default">
 				<?php
				    if($this->session->flashdata('success_msg'))    
				        {           
				    echo "<div class='alert alert-success'>".$this->session->flashdata('success_msg')."</div>"; 
				    }   
				    if($this->session->flashdata('error_msg'))
				        {       
				    echo "<div class='alert alert-danger'>".$this->session->flashdata('error_msg')."</div>";    
				    }           
				?>
 				<div class="panel-heading">
			     	Class Type Details <!-- <a href="<?=base_url()?>travelmodes/createtravelmode" class="btn btn-xs btn-info">Create</a> -->
			    </div>
			    <table id="travelmode" class="table table-striped table-bordered display nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>Sno</th>
							<th>Trvel Mode Type</th>
							<th>Alias Name</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						<?php if($tmodes->num_rows() > 0 ){
							$i=1;
							foreach ($tmodes->result_array() as $modes) { ?>
								<tr>
									<td><?php echo $i;?></td>
									<td><?php echo $modes['mode_type'];?></td>
									<td><?php echo $modes['travel_alias'];?></td>
									<td><a href="<?=base_url()?>travelmode/edittravelmode/<?php echo $modes['id'];?>" class="btn btn-xs btn-primary" title="Edit"><i class="fa fa-pencil" ></i></a></td>
								</tr>
							<?php $i++; }
						}?>
					</tbody>
				</table>
			</div>
		</div>
	</section>
</section>
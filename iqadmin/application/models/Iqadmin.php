<?php 
class Iqadmin extends CI_Model
{
	public function getAllClassTypes($tablename)
	{
		$this->db->select('t.*,m.mode_type');
		$this->db->from($tablename);
		$this->db->join('tbl_travel_modes as m','t.travel_mode_type=m.id','INNER');
		$this->db->order_by('t.id','DESC');
		$query = $this->db->get();
		return $query;
	}

	public function getClasstypeData($tablename,$id)
	{
		$this->db->select("*");
		$this->db->from($tablename);
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->row_array();
	}

	public function getTravelModes($tablename)
	{
		$this->db->select('id,mode_type,travel_alias');
		$this->db->from($tablename);
		$query = $this->db->get();
		return $query;
	}

	public function getClasstypeById($tablename,$id)
	{
		$this->db->select("*");
		$this->db->from($tablename);
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->row_array();
	}

	public function insertData($tablename,$data)
	{
		$this->db->insert($tablename,$data);
		return $this->db->insert_id();
	}

	public function updateData($tablename,$set,$whr)
	{
		$this->db->set($set)->where($whr)->update($tablename);
		return true;
	}

	function deleteData($tablename,$whr)
	{
		$this->db->where($whr)->delete($tablename);
		return true;
	}

	/* Customer Controller */
	public function getAllCustomersData($tablename)
	{
		$this->db->select('id,customer_name, mobile_number, email, address, status');
		$this->db->from($tablename);
		$this->db->where('status',1);
		$query = $this->db->get();
		return $query;
	}

	public function getBookingsById($tablename,$id)
	{
		$this->db->select('b.*,t.id as tid,t.pament_mode,t.amount,t.amount_status,t.reference_number,c.customer_name');
		$this->db->from($tablename);
		$this->db->join('tbl_transaction as t','t.book_id=b.id','INNER');
		$this->db->join('tbl_customers as c','b.customer_id=c.id','INNER');
		$this->db->where('b.customer_id',$id);
		//$this->db->where('b.user_id',$uid);
		$query = $this->db->get();
		return $query;
	}

	public function getBookingAmount($tablename,$cid)
	{
		$this->db->select('');
		$this->db->from($tablename);
		$this->db->where('cust_id',$cid);
		//$this->db->where('user_id',$uid);
		$query = $this->db->get();
		return $query;
	}

	public function getTransactionData($tablename,$id)
	{
		$this->db->select("id,amount,pament_mode,amount_status,reference_number,pay_date");
		$this->db->from($tablename);
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->row_array();
	}


}
?>
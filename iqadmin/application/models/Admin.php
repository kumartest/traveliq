<?php 
class Admin extends CI_Model
{
	public function checkUserLogin($whr,$tablename)
	{
		$this->db->select("*");
		$this->db->from($tablename);
		$this->db->where($whr);
		$query = $this->db->get();
		return $query->result();
	}

	public function insertData($tablename,$data)
	{
		$this->db->insert($tablename,$data);
		return $this->db->insert_id();
	}

	public function updateData($tablename,$set,$whr)
	{
		$this->db->set($set)->where($whr)->update($tablename);
		return true;
	}

	public function getAllUsers($tablename)
	{
		$this->db->select("*");
		$this->db->from($tablename);
		$query = $this->db->get();
		return $query;
	}

	public function getUsersData($tablename,$id)
	{
		$this->db->select("*");
		$this->db->from($tablename);
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->row_array();
	}
}
?>
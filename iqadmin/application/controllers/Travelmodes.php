<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Travelmodes extends CI_Controller {

	public function __construct(){
		parent::__construct();
		is_logged_in();
		$this->load->model("Iqadmin");
	}

	public function index()
	{
		$data['page_title'] = "Travel Modes";
		$data['tmodes'] = $this->Iqadmin->getTravelModes("tbl_travel_modes");
		$this->settemplate->dashboard('travelmodes',$data);
	}

	public function createtravelmode()
	{
		$data['page_title'] = "Create Jouney Type";
		$this->settemplate->dashboard('createtravelmode',$data);
	}
	public function inserttmode()
	{
		$date = date('Y-m-d H:i:s');
		$this->form_validation->set_rules('travelmode','Journey type','trim|required');
		$this->form_validation->set_rules('tmodealias','Mode type','trim|required');
		if($this->form_validation->run() == FALSE){
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
			$this->createtravelmode();
		}else{
			$ctypedata = array('mode_type' => $this->input->post('travelmode'),'travel_alias' => $this->input->post('tmodealias'),'status' => $this->input->post('status'),'created_at' => $date,'updated_at' =>$date );
			$result = $this->Iqadmin->insertData("tbl_travel_modes",$ctypedata);
			if($result > 0){
				$this->session->set_flashdata('success_msg',"Added successfully!");
				redirect('travelmodes');
			}else{
				$this->session->set_flashdata('error_msg',"Adding failed,please try again!");
			}
		}
	}

	public function edittravelmode($id)
	{
		$data['page_title'] = "Edit Jouney type";
		$data['tmodes'] = $this->Iqadmin->getTravelModes("tbl_travel_modes");
		$data['ctypes'] = $this->Iqadmin->getClasstypeById("tbl_journey_types",$id);
		$this->settemplate->dashboard('editjourneytype',$data);
	}

	public function updatejtype($id)
	{
		$date = date('Y-m-d H:i:s');
		$this->form_validation->set_rules('jtypename','Journey type','trim|required');
		$this->form_validation->set_rules('tmodetype','Mode type','trim|required');
		if($this->form_validation->run() == FALSE){
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
			$this->editjourneytype($id);
		}else{
			$uctypedata = array('type_name' => $this->input->post('jtypename'),'travel_mode_type' => $this->input->post('tmodetype'),'status' => $this->input->post('status'),'updated_at' =>$date );
			$whr = array('id'=>$id);
			$result = $this->Iqadmin->updateData("tbl_journey_types",$uctypedata,$whr);
			if($result > 0){
				$this->session->set_flashdata('success_msg',"Updated successfully!");
				redirect('journeytype');
			}else{
				$this->session->set_flashdata('error_msg',"Updataion failed,please try again!");
			}
		}	
	}

	public function deletetravelmode($id)
	{
		$whr = array('id'=>$id);
		$result = $this->Iqadmin->deleteData("tbl_journey_types",$whr);
		if($result > 0){
			$this->session->set_flashdata('success_msg',"Deleted successfully!");
			redirect('journeytype');
		}else{
			$this->session->set_flashdata('error_msg',"Deletion failed,please try again!");
		}
	}

	/*public function changestatus($id,$status)
	{
		$ctype  = $this->Iqadmin->getClasstypeData("tbl_class_types",$id);
		if($ctype){
			$whr = array('id'=>$id);
			$this->db->set(array('status'=>$status))->where($whr)->update("tbl_class_types");
		}
		$this->session->set_flashdata('success_msg',"Status Updated Successfully!");
		redirect('classtype');
	}*/
}

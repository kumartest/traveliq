<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Classtype extends CI_Controller {

	public function __construct(){
		parent::__construct();
		is_logged_in();
		$this->load->model("Iqadmin");
	}

	public function index()
	{
		$data['page_title'] = "Class Types";
		$data['ctypes'] = $this->Iqadmin->getAllClassTypes("tbl_class_types as t");
		$this->settemplate->dashboard('classtype',$data);
	}

	public function createclasstype()
	{
		$data['page_title'] = "Create Class Type";
		$data['tmodes'] = $this->Iqadmin->getTravelModes("tbl_travel_modes");
		$this->settemplate->dashboard('createclasstype',$data);
	}
	public function insertctype()
	{
		$date = date('Y-m-d H:i:s');
		$this->form_validation->set_rules('ctypename','Class type','trim|required');
		$this->form_validation->set_rules('tmodetype','Mode type','trim|required');
		if($this->form_validation->run() == FALSE){
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
			$this->createclasstype();
		}else{
			$ctypedata = array('type_name' => $this->input->post('ctypename'),'travel_mode_type' => $this->input->post('tmodetype'),'status' => $this->input->post('status'),'created_at' => $date,'updated_at' =>$date );
			$result = $this->Iqadmin->insertData("tbl_class_types",$ctypedata);
			if($result > 0){
				$this->session->set_flashdata('success_msg',"Added successfully!");
				redirect('classtype');
			}else{
				$this->session->set_flashdata('error_msg',"Adding failed,please try again!");
			}
		}
	}

	public function editclasstype($id)
	{
		$data['page_title'] = "Edit Class type";
		$data['tmodes'] = $this->Iqadmin->getTravelModes("tbl_travel_modes");
		$data['ctypes'] = $this->Iqadmin->getClasstypeById("tbl_class_types",$id);
		$this->settemplate->dashboard('editclasstype',$data);
	}

	public function updatectype($id)
	{
		$date = date('Y-m-d H:i:s');
		$this->form_validation->set_rules('ctypename','Class type','trim|required');
		$this->form_validation->set_rules('tmodetype','Mode type','trim|required');
		if($this->form_validation->run() == FALSE){
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
			$this->editclasstype($id);
		}else{
			$uctypedata = array('type_name' => $this->input->post('ctypename'),'travel_mode_type' => $this->input->post('tmodetype'),'status' => $this->input->post('status'),'updated_at' =>$date );
			$whr = array('id'=>$id);
			$result = $this->Iqadmin->updateData("tbl_class_types",$uctypedata,$whr);
			if($result > 0){
				$this->session->set_flashdata('success_msg',"Updated successfully!");
				redirect('classtype');
			}else{
				$this->session->set_flashdata('error_msg',"Updataion failed,please try again!");
			}
		}	
	}

	public function deleteclasstype($id)
	{
		$whr = array('id'=>$id);
		$result = $this->Iqadmin->deleteData("tbl_class_types",$whr);
		if($result > 0){
			$this->session->set_flashdata('success_msg',"Deleted successfully!");
			redirect('classtype');
		}else{
			$this->session->set_flashdata('error_msg',"Deletion failed,please try again!");
		}
	}

	/*public function changestatus($id,$status)
	{
		$ctype  = $this->Iqadmin->getClasstypeData("tbl_class_types",$id);
		if($ctype){
			$whr = array('id'=>$id);
			$this->db->set(array('status'=>$status))->where($whr)->update("tbl_class_types");
		}
		$this->session->set_flashdata('success_msg',"Status Updated Successfully!");
		redirect('classtype');
	}*/
}

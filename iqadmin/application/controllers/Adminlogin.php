<?php 
//ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');
class Adminlogin extends CI_Controller {
	public function __Construct()
	{
		parent::__Construct();
		$this->load->model('Admin');
	}

	public function index()
	{
		$data['title'] = "Login";
		if($this->session->userdata('t_loggedin')){
			redirect('dashboard');
		}
		$this->settemplate->login("",$data);
	}

	public function login()
	{
		$data["page_title"] = "Login";
		$this->form_validation->set_rules('adminmobile','Mobile Number','trim|required');
		$this->form_validation->set_rules('adminpwd','Password','trim|required');
		if ($this->form_validation->run() == FALSE)
		{	
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
			$this->index();
		}else{
			$umobile = $this->input->post('adminmobile');
			$upwd = $this->input->post('adminpwd');

			$whr = array('mobile_number' => $umobile,'admin_password' => $upwd , 'status' => 1);
			$result = $this->Admin->checkUserLogin($whr,"tbl_admin");

			if(count($result) > 0){
				$this->session->set_userdata('admin_id',$result[0]->id);
				$this->session->set_userdata('admin_moblie',$result[0]->mobile_number);
				$this->session->set_userdata('admin_name',$result[0]->admin_name);
				$this->session->set_userdata('admin_email',$result[0]->email);
				$this->session->set_userdata('role',$result[0]->role);
				$this->session->set_userdata('t_loggedin',TRUE);
				redirect('dashboard');
			}else{
				$this->session->set_flashdata('error_msg',"Invalid Email/Password");
				redirect('adminlogin');
			}
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		$this->session->unset_userdata('admin_id');
		$this->session->unset_userdata('admin_moblie');
		$this->session->unset_userdata('admin_name');
		$this->session->unset_userdata('admin_email');
		$this->session->unset_userdata('role');
		$this->session->unset_userdata('t_loggedin');
		redirect('/');
	}

}
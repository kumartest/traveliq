<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct(){
		parent::__construct();
		is_logged_in();
		$this->load->model("Admin");
	}

	public function index()
	{
		$data['page_title'] = "Dashboard";
		$data['users'] = $this->Admin->getAllUsers("tbl_users");
		$this->settemplate->dashboard('dashboard',$data);
	}
}

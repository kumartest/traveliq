<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller {

	public function __construct(){
		parent::__construct();
		is_logged_in();
		$this->load->model("Iqadmin");
	}

	public function index()
	{
		$data['page_title'] = "Customers Data"; 
		$data['customers'] = $this->Iqadmin->getAllCustomersData("tbl_customers");
		$this->settemplate->dashboard("customer",$data);
	}

	public function shwotransactions($id)
	{
		$data['page_title'] = "Customers Transactions"; 
	
		//$uid = $this->user;
		$pending = 0; $paid = 0; $rejected = 0; $tot_amt = 0;
		//$id = $_GET['id'];
		$data['result'] = $this->Iqadmin->getBookingsById("tbl_booking as b",$id)->result_array();
		$get_amount = $this->Iqadmin->getBookingAmount("tbl_transaction",$id);
		if($get_amount->num_rows() >0){
			foreach ($get_amount->result_array() as $amt) {
				$tot_amt += $amt['amount'];
				if($amt['amount_status'] == 0){
					$paid += $amt['amount'];
				}else if($amt['amount_status'] == 1) {
					$pending += $amt['amount'];
				}else{
					$rejected += $amt['amount'];
				}
			}
		}
		$data['amounts'] = array('tot_amount' => $tot_amt,'pending' => $pending,'paid' => $paid,'rejected' => $rejected);	
		$this->settemplate->dashboard("customer_transactions",$data);
	}

	public function updatestatus($id,$status)
	{
		$tdata  = $this->Iqadmin->getTransactionData("tbl_transaction",$id);
		if($tdata){
			$whr = array('id'=>$id);
			$this->db->set(array('amount_status'=>$status))->where($whr)->update("tbl_transaction");
		}
		$this->session->set_flashdata('success_msg',"Status Updated Successfully!");
		redirect('customer/shwotransactions/'.$id);
	}

	public function edittransaction($id)
	{
		$data['page_title'] = "Update Transaction";
		$data['tdata']  = $this->Iqadmin->getTransactionData("tbl_transaction",$id);
		$this->settemplate->dashboard("update_transaction",$data);
	}

	public function updatetnx($id)
	{
		$ptype = $this->input->post('payment_type');
		$data = array();
		$whr = array('id'=>$id);
		if($ptype == 0){
			$data = array('pament_mode'=> $ptype,'reference_number'=> "",'pay_date'=> $this->input->post('payment_date'),'amount_status' =>$this->input->post('amt_status'));
		}else if($ptype == 1){
			$data = array('pament_mode'=> $ptype,'reference_number'=> $this->input->post('cheque_number'),'pay_date'=> $this->input->post('payment_date'),'amount_status' =>1);
		}else{
			$data = array('pament_mode'=> $ptype,'reference_number'=> $this->input->post('bank_ref_num'),'pay_date'=> $this->input->post('payment_date'),'amount_status' =>$this->input->post('amt_status'));
		}

		$result = $this->db->set($data)->where($whr)->update("tbl_transaction");
		if($result > 0){
			$this->session->set_flashdata('success_msg',"Added successfully!");
			redirect('customer/shwotransactions/'.$id);
		}else{
			$this->session->set_flashdata('error_msg',"Adding failed,please try again!");
		}
		redirect('customer/edittransaction/'.$id);
	}
}
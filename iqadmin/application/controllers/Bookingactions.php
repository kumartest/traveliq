<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bookingactions extends CI_Controller {

	public function __construct(){
		parent::__construct();
		is_logged_in();
		$this->load->model("Iqadmin");
	}

	public function index()
	{
		$data['page_title'] = "Bookings"; 

		$this->settemplate->dashboard("booking",$data);
	}
}
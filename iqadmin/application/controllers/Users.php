<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	public function __construct(){
		parent::__construct();
		is_logged_in();
		$this->load->model('Admin');
	}

	public function index()
	{
		$data['page_title'] = "Users";
		$data['users'] = $this->Admin->getAllUsers("tbl_users");
		$this->settemplate->dashboard('users',$data);
	}

	public function createUser()
	{
		$data['page_title'] = "Create User";
		$this->settemplate->dashboard('usercreate',$data);
	}

	public function insertUser()
	{
		$date = date('Y-m-d H:i:s');
		$this->form_validation->set_rules('username','User Name','trim|required');
		$this->form_validation->set_rules('email','Email','trim|required');
		$this->form_validation->set_rules('user_pwd','Password','trim|required');
		$this->form_validation->set_rules('mobilenumber','Mobile Number','trim|required');
		if($this->form_validation->run() == FALSE){
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
			$this->createUser();
		}else{
			$userData = array(
				'user_name' => $this->input->post("username"), 
				'email_id' => $this->input->post("email"), 
				'password' => $this->input->post("user_pwd"), 
				'mobile_number' => $this->input->post("mobilenumber"), 
				'address' => $this->input->post("address"), 
				'status' => $this->input->post("status"), 
				'created_at' => $date, 
				'updated_at' => $date, 
			);
			$lastId = $this->Admin->insertData("tbl_users",$userData);

			if($lastId > 0){
				$this->session->set_flashdata('success_msg',"Added successfully!");
				redirect('users');
			}else{
				$this->session->set_flashdata('error_msg',"Adding failed,please try again!");
			}
		}
	}

	public function editUser($id)
	{
		$data['page_title'] = "Edit User";
		$data['users'] = $this->Admin->getUsersData("tbl_users",$id);
		$this->settemplate->dashboard('useredit',$data);
	}

	public function updateUser($id)
	{
		$date = date('Y-m-d H:i:s');
		$this->form_validation->set_rules('username','User Name','trim|required');
		$this->form_validation->set_rules('email','Email','trim|required');
		$this->form_validation->set_rules('user_pwd','Password','trim|required');
		$this->form_validation->set_rules('mobilenumber','Mobile Number','trim|required');
		if($this->form_validation->run() == FALSE){
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
			$this->editUser($id);
		}else{
			$userData = array(
				'user_name' => $this->input->post("username"), 
				'email_id' => $this->input->post("email"), 
				'password' => $this->input->post("user_pwd"), 
				'mobile_number' => $this->input->post("mobilenumber"), 
				'address' => $this->input->post("address"), 
				'status' => $this->input->post("status"), 
				'updated_at' => $date, 
			);

			$whr = array("id"=>$id);
			$result = $this->Admin->updateData("tbl_users",$userData,$whr);
			if($result){
				$this->session->set_flashdata('success_msg',"Updated successfully!");
				redirect('users');
			}else{
				$this->session->set_flashdata('error_msg',"Updation failed,please try again!");
			}

		}
	}

	public function changestatus($uid,$status)
	{
		$udata  = $this->Admin->getUsersData("tbl_users",$uid);
		if($udata){
			$whr = array('id'=>$uid);
			$this->db->set(array('status'=>$status))->where($whr)->update("tbl_users");
		}
		$this->session->set_flashdata('success_msg',"Status Updated Successfully!");
		redirect('users');
	}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Journeytype extends CI_Controller {

	public function __construct(){
		parent::__construct();
		is_logged_in();
		$this->load->model("Iqadmin");
	}

	public function index()
	{
		$data['page_title'] = "Jouney Types";
		$data['ctypes'] = $this->Iqadmin->getAllClassTypes("tbl_journey_types as t");
		$this->settemplate->dashboard('journeytype',$data);
	}

	public function createjourneytype()
	{
		$data['page_title'] = "Create Jouney Type";
		$data['tmodes'] = $this->Iqadmin->getTravelModes("tbl_travel_modes");
		$this->settemplate->dashboard('createjourneytype',$data);
	}
	public function insertjtype()
	{
		$date = date('Y-m-d H:i:s');
		$this->form_validation->set_rules('jtypename','Journey type','trim|required');
		$this->form_validation->set_rules('tmodetype','Mode type','trim|required');
		if($this->form_validation->run() == FALSE){
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
			$this->createclasstype();
		}else{
			$ctypedata = array('type_name' => $this->input->post('jtypename'),'travel_mode_type' => $this->input->post('tmodetype'),'status' => $this->input->post('status'),'created_at' => $date,'updated_at' =>$date );
			$result = $this->Iqadmin->insertData("tbl_journey_types",$ctypedata);
			if($result > 0){
				$this->session->set_flashdata('success_msg',"Added successfully!");
				redirect('journeytype');
			}else{
				$this->session->set_flashdata('error_msg',"Adding failed,please try again!");
			}
		}
	}

	public function editjourneytype($id)
	{
		$data['page_title'] = "Edit Jouney type";
		$data['tmodes'] = $this->Iqadmin->getTravelModes("tbl_travel_modes");
		$data['ctypes'] = $this->Iqadmin->getClasstypeById("tbl_journey_types",$id);
		$this->settemplate->dashboard('editjourneytype',$data);
	}

	public function updatejtype($id)
	{
		$date = date('Y-m-d H:i:s');
		$this->form_validation->set_rules('jtypename','Journey type','trim|required');
		$this->form_validation->set_rules('tmodetype','Mode type','trim|required');
		if($this->form_validation->run() == FALSE){
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
			$this->editjourneytype($id);
		}else{
			$uctypedata = array('type_name' => $this->input->post('jtypename'),'travel_mode_type' => $this->input->post('tmodetype'),'status' => $this->input->post('status'),'updated_at' =>$date );
			$whr = array('id'=>$id);
			$result = $this->Iqadmin->updateData("tbl_journey_types",$uctypedata,$whr);
			if($result > 0){
				$this->session->set_flashdata('success_msg',"Updated successfully!");
				redirect('journeytype');
			}else{
				$this->session->set_flashdata('error_msg',"Updataion failed,please try again!");
			}
		}	
	}

	public function deletejourneytype($id)
	{
		$whr = array('id'=>$id);
		$result = $this->Iqadmin->deleteData("tbl_journey_types",$whr);
		if($result > 0){
			$this->session->set_flashdata('success_msg',"Deleted successfully!");
			redirect('journeytype');
		}else{
			$this->session->set_flashdata('error_msg',"Deletion failed,please try again!");
		}
	}

	/*public function changestatus($id,$status)
	{
		$ctype  = $this->Iqadmin->getClasstypeData("tbl_class_types",$id);
		if($ctype){
			$whr = array('id'=>$id);
			$this->db->set(array('status'=>$status))->where($whr)->update("tbl_class_types");
		}
		$this->session->set_flashdata('success_msg',"Status Updated Successfully!");
		redirect('classtype');
	}*/
}

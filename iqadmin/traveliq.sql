-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 01, 2018 at 08:28 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `traveliq`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_booking`
--

CREATE TABLE `tbl_booking` (
  `id` int(11) NOT NULL,
  `customer_id` varchar(100) NOT NULL,
  `dob` date NOT NULL,
  `doj` date NOT NULL,
  `no_of_pax` int(4) NOT NULL,
  `source` varchar(100) NOT NULL,
  `destination` varchar(100) NOT NULL,
  `travel_mode` varchar(100) NOT NULL,
  `travel_name` varchar(100) NOT NULL,
  `class_type` varchar(100) NOT NULL,
  `pnr_number` varchar(100) NOT NULL,
  `booking_fare` decimal(10,2) NOT NULL DEFAULT '0.00',
  `comission` decimal(10,2) NOT NULL DEFAULT '0.00',
  `total_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `referal` varchar(225) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_booking`
--

INSERT INTO `tbl_booking` (`id`, `customer_id`, `dob`, `doj`, `no_of_pax`, `source`, `destination`, `travel_mode`, `travel_name`, `class_type`, `pnr_number`, `booking_fare`, `comission`, `total_amount`, `referal`, `user_id`, `created_at`, `updated_at`) VALUES
(1, '1', '2018-01-30', '2018-02-05', 1, 'visakhapatnam', 'secendrabad', 'flight', 'testing', 'ECONOMY', 'AP31CM6963', '10000.00', '500.00', '10500.00', 'by kiran', 1, '2018-01-30 14:08:29', '2018-02-01 06:53:50'),
(2, '1', '2018-01-31', '2018-02-06', 2, 'secendrabad', 'vizag', 'Cab', 'testing', 'ECONOMY', 'AP31CM6963', '15000.00', '500.00', '15500.00', 'test', 1, '2018-01-30 14:39:11', '2018-02-01 06:53:44'),
(3, '1', '2018-01-30', '2018-02-05', 1, 'visakhapatnam', 'secendrabad', 'bus', 'testing', 'ECONOMY', 'AP31CM6963', '1000.00', '150.00', '1650.00', 'testing', 2, '2018-01-31 12:18:41', '2018-02-01 06:53:38'),
(4, '1', '2018-02-01', '2018-02-06', 1, 'visakhapatnam', 'secendrabad', 'Cab', 'testing', 'ECONOMY', 'AP31CM6963', '500.00', '150.00', '650.00', 'kiran', 1, '2018-02-01 07:52:58', '2018-02-01 07:11:34'),
(5, '1', '2018-02-01', '2018-02-05', 1, 'secendrabad', 'vizag', 'Cab', 'testing', 'dfsd', 'AP31CM6963', '200.00', '20.00', '220.00', 'lksdf', 1, '2018-02-01 08:11:46', '2018-02-01 02:41:46');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_customers`
--

CREATE TABLE `tbl_customers` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(225) NOT NULL,
  `mobile_number` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_customers`
--

INSERT INTO `tbl_customers` (`id`, `customer_name`, `mobile_number`, `email`, `address`, `status`, `created_at`, `updated_at`) VALUES
(1, 'kirankumar', '9494975507', 'chinthadakirankumar@gmail.com', 'botu street\r\nparvathipuram', 1, '2018-01-31 13:14:26', '2018-01-31 12:39:17');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(225) NOT NULL,
  `email_id` varchar(100) NOT NULL,
  `password` varchar(225) NOT NULL,
  `mobile_number` varchar(20) NOT NULL,
  `address` text NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '''1''->''Active'',''0''->''Inactive''',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_name`, `email_id`, `password`, `mobile_number`, `address`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Kiran kumar', 'kiran@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '9494975506', 'vizag', 1, '2018-01-30 00:00:00', '2018-01-30 10:43:30'),
(2, 'Ravi kumar', 'ravi@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '9494975507', 'vizag', 1, '2018-01-30 00:00:00', '2018-01-30 10:43:30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_booking`
--
ALTER TABLE `tbl_booking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_customers`
--
ALTER TABLE `tbl_customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_booking`
--
ALTER TABLE `tbl_booking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_customers`
--
ALTER TABLE `tbl_customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

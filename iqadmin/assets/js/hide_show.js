
 $(function() {
    $('#car_Connect_Show').hide();
    $('#car_Rental_show').hide();
	$('#DriverHire_show').hide();
	$('#car_Showroom_show').hide();
	$('#used_Car_show').hide();
	$('#tours_Travels_show').hide();
	$('#car_Decares_show').hide();
	$('#insurance_show').hide();
	$('#finance_show').hide();
	$('#hotels_show').hide();
	
    $('#adType').change(function(){
        if($('#adType').val() == '1') 
		{
           
			$('#car_Connect_Show').show();
			$('#car_Rental_show').hide();
			$('#DriverHire_show').hide();
			$('#car_Showroom_show').hide();
			$('#used_Car_show').hide();
			$('#tours_Travels_show').hide();
			$('#car_Decares_show').hide();
			$('#insurance_show').hide();
			$('#finance_show').hide();
			$('#hotels_show').hide();
        }
        else if($('#adType').val() == '2')
		 {
			$('#car_Connect_Show').hide();
			$('#car_Rental_show').show();
			$('#DriverHire_show').hide();
			$('#car_Showroom_show').hide();
			$('#used_Car_show').hide();
			$('#tours_Travels_show').hide();
			$('#car_Decares_show').hide();
			$('#insurance_show').hide();
			$('#finance_show').hide();
			$('#hotels_show').hide();
		 }
		 
		 else if($('#adType').val() == '3')
		 {
			$('#car_Connect_Show').hide();
			$('#car_Rental_show').hide();
			$('#DriverHire_show').show();
			$('#car_Showroom_show').hide();
			$('#used_Car_show').hide();
			$('#tours_Travels_show').hide();
			$('#car_Decares_show').hide();
			$('#insurance_show').hide();
			$('#finance_show').hide();
			$('#hotels_show').hide();
		 }
		 
		 else if($('#adType').val() == '4')
		 {
			$('#car_Connect_Show').hide();
			$('#car_Rental_show').hide();
			$('#DriverHire_show').hide();
			$('#car_Showroom_show').show();
			$('#used_Car_show').hide();
			$('#tours_Travels_show').hide();
			$('#car_Decares_show').hide();
			$('#insurance_show').hide();
			$('#finance_show').hide();
			$('#hotels_show').hide();
		 }
		 
		 else if($('#adType').val() == '5')
		 {
			$('#car_Connect_Show').hide();
			$('#car_Rental_show').hide();
			$('#DriverHire_show').hide();
			$('#car_Showroom_show').hide();
			$('#used_Car_show').show();
			$('#tours_Travels_show').hide();
			$('#car_Decares_show').hide();
			$('#insurance_show').hide();
			$('#finance_show').hide();
			$('#hotels_show').hide(); 
		 }
		 
		 
		 else if($('#adType').val() == '6')
		 {
			$('#car_Connect_Show').hide();
			$('#car_Rental_show').hide();
			$('#DriverHire_show').hide();
			$('#car_Showroom_show').hide();
			$('#used_Car_show').hide();
			$('#tours_Travels_show').show();
			$('#car_Decares_show').hide();
			$('#insurance_show').hide();
			$('#finance_show').hide();
			$('#hotels_show').hide(); 
		 }
		 
		 
		 else if($('#adType').val() == '7')
		 {
			$('#car_Connect_Show').hide();
			$('#car_Rental_show').hide();
			$('#DriverHire_show').hide();
			$('#car_Showroom_show').hide();
			$('#used_Car_show').hide();
			$('#tours_Travels_show').hide();
			$('#car_Decares_show').show();
			$('#insurance_show').hide();
			$('#finance_show').hide();
			$('#hotels_show').hide();
		 }
		 
		else if($('#adType').val() == '8')
		 {
			$('#car_Connect_Show').hide();
			$('#car_Rental_show').hide();
			$('#DriverHire_show').hide();
			$('#car_Showroom_show').hide();
			$('#used_Car_show').hide();
			$('#tours_Travels_show').hide();
			$('#car_Decares_show').hide();
			$('#insurance_show').show();
			$('#finance_show').hide();
			$('#hotels_show').hide();
		 }
		 
		else if($('#adType').val() == '9')
		 {
			$('#car_Connect_Show').hide();
			$('#car_Rental_show').hide();
			$('#DriverHire_show').hide();
			$('#car_Showroom_show').hide();
			$('#used_Car_show').hide();
			$('#tours_Travels_show').hide();
			$('#car_Decares_show').hide();
			$('#insurance_show').hide();
			$('#finance_show').show();
			$('#hotels_show').hide();
		 }

		else {
            
			$('#car_Connect_Show').hide();
			$('#car_Rental_show').hide();
			$('#DriverHire_show').hide();
			$('#car_Showroom_show').hide();
			$('#used_Car_show').hide();
			$('#tours_Travels_show').hide();
			$('#car_Decares_show').hide();
			$('#insurance_show').hide();
			$('#finance_show').hide();
			$('#hotels_show').show();
			
        } 
    });
	
	
	
	
	
});
$(document).ready(function(){
	$('.amtchange').on('change',function(){
        var amt = $('#bookingfare').val();
        var com = $('#comission').val();
        var tot = parseInt(amt)+parseInt(com);
        $("#totalamount").val(tot);
        $("#amount").val(tot);
    });
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd'
    });
    $('.dateofjourney').datepicker({
        format: 'yyyy-mm-dd'
    });
	/* add new customer */
    $(".addnewcust").bind("click", function () {
        var div = $("<div />");
        div.attr('id','removediv');
        div.html(GetDynamicTextBox(""));
        $("#addcust").append(div);
    });

    $("body").on("click", "#deletecontacts", function () {
        $(this).closest("#removediv").remove();
    });

    function GetDynamicTextBox(value) 
    {
        return '<div class="col-sm-12"><div class="row"><div class="form-group col-sm-3"><input type="text" class="ggg" name="subcust[]" id="subcust" placeholder="Enter Customer Name"></div><div class="form-group col-sm-3"><input type="text" class="ggg" name="sub_mobile[]" id="sub_mobile" placeholder="Enter Mobile Number"></div><div class="form-group col-sm-3"><input type="text" class="ggg" name="sub_address[]" id="sub_address" placeholder="Enter Address"></div><div class="form-group col-sm-3"><span class="btn btn-danger addnewcust" id="deletecontacts" data-widget="add"><i class="fa fa-times-circle"></i></span></div></div></div>';
    }

    ///* for tabs */
    $('#flight').hide();
    $('#train').hide();
    $('#hotel').hide();
    $('#cab').hide();

    

    /* Flight */
    $('#check_flight').click(function() {
        $("#flight").toggle(this.checked);
            if($('#check_flight').prop('checked')){
                $("#flight").show();
            } else {
                $("#flight").hide();
              } 
    });
    /* Train */
    $('#check_train').click(function() {
        $("#train").toggle(this.checked);
            if($('#check_train').prop('checked')){
                $("#train").show();
            } else {
                $("#train").hide();
              } 
    });
    /* Hotel */
    $('#check_hotel').click(function() {
        $("#hotel").toggle(this.checked);
            if($('#check_hotel').prop('checked')){
                $("#hotel").show();
            } else {
                $("#hotel").hide();
              } 
    });
    /* Cab */
    $('#check_cab').click(function() {
        $("#cab").toggle(this.checked);
            if($('#check_cab').prop('checked')){
                $("#cab").show();
            } else {
                $("#cab").hide();
              } 
    });

    $("#payment_type").change(function()
    {
        if($(this).val() == "0")
        {
            $("#pay_cheque").hide();
            $("#bank_ref_num").hide();
        }
        else if($(this).val() == "1")
        {
            $("#bank_ref_num").hide();
            $("#pay_cheque").show();
        }else{
            $("#pay_cheque").hide();
            $("#bank_ref_num").show();
        }
    });

    $('.book_res').on('change', function() {
        var id = this.value;
        var url = baseurl+"bookingactions/getbooking_results";
        $.ajax({
            type: 'GET',
            url: url,
            data:{id:id},
            beforeSend: function()
            {
                $("#viewdata").html("Please wait...");
            },
            success: function (result) {
                if(result){
                    $("#viewdata").html(result);
                }
            }

        });
    });
});



function vaidate_bookingform()
{
    if(document.getElementById('cust_name').value == '')
    {
        document.getElementById("formerrors").setAttribute("class", "alert alert-danger");
        document.getElementById('formerrors').innerHTML = 'Please enter Customer Name';
        document.bookingform.cust_name.focus();
        return false;
    }else if(document.getElementById('dob').value == ''){
        document.getElementById("formerrors").setAttribute("class", "alert alert-danger");
        document.getElementById('formerrors').innerHTML = 'Please enter Customer Name';
        document.bookingform.dob.focus();
        return false;
    }else if(document.getElementById('subcust').value == ''){
        document.getElementById("formerrors").setAttribute("class", "alert alert-danger");
        document.getElementById('formerrors').innerHTML = 'Please enter User Name';
        document.bookingform.subcust.focus();
        return false;
    }else if(document.getElementById('sub_mobile').value == ''){
        document.getElementById("formerrors").setAttribute("class", "alert alert-danger");
        document.getElementById('formerrors').innerHTML = 'Please enter user mobile';
        document.bookingform.sub_mobile.focus();
        return false;
    }else if(document.getElementById('sub_address').value == ''){
        document.getElementById("formerrors").setAttribute("class", "alert alert-danger");
        document.getElementById('formerrors').innerHTML = 'Please enter user address';
        document.bookingform.sub_address.focus();
        return false;
    }else if(document.getElementById('bookingfare').value < 0){
        document.getElementById("formerrors").setAttribute("class", "alert alert-danger");
        document.getElementById('formerrors').innerHTML = 'Please enter fare amount';
        document.bookingform.bookingfare.focus();
        return false;
    }else if(document.getElementById('referal').value == ''){
        document.getElementById("formerrors").setAttribute("class", "alert alert-danger");
        document.getElementById('formerrors').innerHTML = 'Please enter reference name';
        document.bookingform.referal.focus();
        return false;
    }else{
        return true;
    }



}
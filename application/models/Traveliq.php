<?php 
class Traveliq extends CI_Model
{
	public function checkUserLogin($whr,$tablename)
	{
		$this->db->select("*");
		$this->db->from($tablename);
		$this->db->where($whr);
		$query = $this->db->get();
		return $query->result();
	}

	public function insertData($tablename,$data)
	{
		$this->db->insert($tablename,$data);
		return $this->db->insert_id();
	}

	public function updateData($tablename,$data,$whr)
	{
		$this->db->set($data)->where($whr)->update($tablename);
		return true;
	}


	public function getAllBookings($tablename,$uid)
	{
		$this->db->select('tbl_booking.*,tbl_customers.customer_name as cname,tbl_travel_modes.mode_type as modetype');
		$this->db->from($tablename);
		$this->db->join('tbl_customers','tbl_booking.customer_id=tbl_customers.id','INNER');
		$this->db->join('tbl_travel_modes','tbl_booking.mode_type=tbl_travel_modes.id','INNER');
		$this->db->where('tbl_booking.user_id',$uid);
		$query = $this->db->get();
		return $query;
	}

	public function getCustomersData($tablename)
	{
		$this->db->select('*');
		$this->db->from($tablename);
		$query = $this->db->get();
		return $query;
	}

	public function getCustomer($tablename)
	{
		$this->db->select('id,customer_name,mobile_number,email');
		$this->db->from($tablename);
		//$this->db->where('id',$id);
		$query = $this->db->get();
		return $query;
	}

	public function getTravelModes($tablename)
	{
		$this->db->select('id,mode_type');
		$this->db->from($tablename);
		$query = $this->db->get();
		return $query;
	}

	public function getJourneyType($tablename,$mtype)
	{
		$this->db->select('id,type_name');
		$this->db->from($tablename);
		$this->db->where('travel_mode_type',$mtype);
		$query = $this->db->get();
		return $query;
	}

	public function getBookingDetails($id)
	{
		$this->db->select('b.*,t.id,t.pament_mode,t.account_type,t.amount,t.amount_status,t.reference_number,t.pay_date,c.customer_name,a.type_name');
		$this->db->from('tbl_booking as b');
		$this->db->join('tbl_transaction as t','b.id=t.book_id','INNER');
		$this->db->join('tbl_customers as c','b.customer_id=c.id','INNER');
		$this->db->join('tbl_account_type as a','t.account_type=a.id','INNER');
		$this->db->where('b.id',$id);
		$this->db->order_by('t.id','ASC');
		$query = $this->db->get();
		return $query->result_array();
	}

	public function getAmountDetailsById($id,$account_type)
	{
		$res = 0;
		$this->db->select('t.amount');
		$this->db->from("tbl_transaction as t");
		$this->db->where('t.book_id',$id);
		$this->db->where('t.account_type',$account_type);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			foreach ($query->result_array() as $amt) {
				$res = $res+$amt['amount'];
			}
			return $res;
		}else{
			return $res;
		}
	}

	public function getBookingUsers($id,$uid)
	{
		$this->db->select('id,sub_cust_name,mobile_nuber,age,address,status');
		$this->db->from("tbl_sub_customers");
		$this->db->where('book_id',$id);
		$this->db->where('cust_id',$uid);
		$query = $this->db->get();
		/*print_r($this->db->last_query());
		exit;*/
		return $query;
	}

	public function getCustomerDetails($id)
	{
		$this->db->select('id,customer_name,mobile_number,email,address,status');
		$this->db->from("tbl_customers");
		$this->db->where('id',$id);
		$query = $this->db->get()->row_array();
		return $query;
	}

	public function getBookingsById($tablename,$id,$uid)
	{
		$this->db->select('b.*,t.id as tid,t.pament_mode,t.amount,t.amount_status,t.reference_number,c.customer_name');
		$this->db->from($tablename);
		$this->db->join('tbl_transaction as t','t.book_id=b.id','INNER');
		$this->db->join('tbl_customers as c','b.customer_id=c.id','INNER');
		$this->db->where('b.customer_id',$id);
		$this->db->where('b.user_id',$uid);
		$query = $this->db->get();
		return $query;
	}

	public function getBookingAmount($tablename,$cid,$uid)
	{
		$this->db->select('');
		$this->db->from($tablename);
		$this->db->where('cust_id',$cid);
		$this->db->where('user_id',$uid);
		$query = $this->db->get();
		return $query;
	}



}
?>
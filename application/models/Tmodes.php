<?php 
class Tmodes extends CI_Model
{

	public function getAllTravelsData($tablename,$uid)
	{
		$this->db->select("t.*,c.customer_name");
		$this->db->from($tablename);
		$this->db->join('tbl_customers as c','t.cust_id=c.id','INNER');
		$this->db->where('t.user_id',$uid);
		$query = $this->db->get();
		return $query;
	}

	public function insertData($tablename,$data)
	{
		$this->db->insert($tablename,$data);
		return $this->db->insert_id();
	}

	public function updateData($tablename,$data,$whr)
	{
		$this->db->set($data)->where($whr)->update($tablename);
		return true;
	}
}
?>
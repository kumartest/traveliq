<style type="text/css">
    .alert{
        margin-bottom:5px !important;
        padding:2px 8px !important;
    }
</style>
<section id="main-content">
	<section class="wrapper">
    	<!-- <div class="form-w3layouts"> -->
            <div class="row">
                <div class="col-lg-12 butn">
                    <div id="formerrors"></div>
                    <?php echo validation_errors();?>
                    <?php
                        if($this->session->flashdata('success_msg'))    
                            {           
                        echo "<div class='alert alert-success'>".$this->session->flashdata('success_msg')."</div>"; 
                        }   
                        if($this->session->flashdata('error_msg'))
                            {       
                        echo "<div class='alert alert-danger'>".$this->session->flashdata('error_msg')."</div>";    
                        }           
                    ?>
                    <section>
                        <header class="panel-heading">
                            Booking Form
                        </header>
                        <div class="panel-body">
                            <form role="form" action="<?=base_url()?>bookingactions/store" method="post" name="bookingform" id="bookingform" onsubmit="return vaidate_bookingform();">
                                <div class="form-group col-sm-6">
                                    <label for="cust_name">Customer Name</label>
                                    <select  class="ggg" name="cust_name" id="cust_name">
                                        <option value="">Select</option> 
                                        <?php if($customer->num_rows() > 0){
                                            foreach ($customer->result_array() as $cust) { ?>
                                                <option value="<?php echo $cust['id']?>"><?php echo $cust['customer_name']?></option>
                                            <?php }
                                        }?>
                                    </select>
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="dob">Date of Booking</label>
                                    <input type="text" class="ggg datepicker" name="dob" id="dob" data-provide="datepicker" data-date-format="yyyy-mm-dd" placeholder="Enter Date of Booking">
                                </div>
                                <div class="col-sm-12">
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li class="active">
                                                <a href="#home" aria-controls="home" role="tab" data-toggle="tab">Travel Mode</a>
                                            </li>
                                            <li id="flight" style="display: none;">
                                                <a href="#flight_sub" aria-controls="flight_sub" role="tab" data-toggle="tab">Flight</a>
                                            </li>
                                            <li id="train" style="display: none;">
                                                <a href="#train_sub" aria-controls="train_sub" role="tab" data-toggle="tab">Train</a>
                                            </li>
                                            <li id="hotel" style="display: none;">
                                                <a href="#hotels_sub" aria-controls="hotels_sub" role="tab" data-toggle="tab">Hotel</a>
                                            </li>
                                            <li id="cab" style="display: none;">
                                                <a href="#cab_sub" aria-controls="cab_sub" role="tab" data-toggle="tab">Cab</a>
                                            </li>
                                        </ul>
                                        <!-- Tab panes -->
                                        <div class="tab-content m-top">
                                            <div role="tabpanel" class="tab-pane active" id="home">
                                                <div>
                                                    <div class="col-sm-3 m-bottom">
                                                        <div class="checkbox">
                                                            <label class="fnt-weight">
                                                                <input type="checkbox" class="tmode" name="tmode[]" id="check_flight" value="1">Flights
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="checkbox">
                                                            <label class="fnt-weight">
                                                                <input type="checkbox" class="tmode" name="tmode[]" id="check_train" value="2">Train
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="checkbox">
                                                            <label class="fnt-weight">
                                                                <input type="checkbox" class="tmode" name="tmode[]" id="check_hotel" value="3">Hotel
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="checkbox">
                                                            <label class="fnt-weight">
                                                                <input type="checkbox" class="tmode" name="tmode[]" id="check_cab" value="4">Cab
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="flight_sub">
                                                <!-- Flight Data -->
                                                <div class="form-group col-sm-6">
                                                    <label for="ttype">Type</label>
                                                    <select class="ggg" name="ttype" id="ttype">
                                                        <option value="1">Domestic</option>
                                                        <option value="2">International</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="journeytype">Journey Type</label>
                                                    <select class="ggg" name="journeytype" id="journeytype">
                                                        <option value="2">Select Journey Type</option>
                                                        <?php if($fjtype->num_rows() > 0){
                                                            foreach ($fjtype->result_array() as $fres) { ?>
                                                                <option value="<?php echo $fres['id'];?>"><?php echo $fres['type_name'];?></option>        
                                                            <?php }
                                                        }?>
                                                    </select>
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="doj">Date of Journey</label>
                                                    <input type="text" class="ggg dateofjourney" name="doj" id="doj" data-provide="datepicker" data-date-format="yyyy-mm-dd" placeholder="Enter Date of Journey">
                                                </div>
                                                <div class="form-group  col-sm-3">
                                                    <label for="time1">Time</label>
                                                    <input type="text" class="col-sm-6 ggg" name="time1" id="time1"  placeholder="HH:MM">
                                                </div>
                                                <div class="form-group  col-sm-3">
                                                    <label for="time2">&nbsp;</label>
                                                    <input type="text" class="col-sm-6 ggg" name="time2" id="time2" placeholder="HH:MM">
                                                   
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="f_souce">Source</label>
                                                    <input type="text" class="ggg" name="f_souce" id="f_souce" placeholder="Enter Flight Source">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="f_destination">Destination</label>
                                                    <input type="text" class="ggg" name="f_destination" id="f_destination" placeholder="Enter Flight Destination">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="clstype">Class Type</label>
                                                    <select class="ggg" name="clstype" id="clstype">
                                                        <option value="">Select Class Type</option>
                                                        <?php if($fctype->num_rows() > 0){
                                                            foreach ($fctype->result_array() as $fct) { ?>
                                                                <option value="<?php echo $fct['id'];?>"><?php echo $fct['type_name'];?></option>
                                                            <?php }
                                                        }?>
                                                    </select>
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="noofpax">No of Pax</label>
                                                    <input type="text" class="ggg" name="noofpax" id="noofpax" placeholder="Enter No of Pax">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="flight_name">Flight Name</label>
                                                    <input type="text" class="ggg" name="flight_name" id="flight_name" placeholder="Enter Flight Name">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="pnrnumber">PNR Number</label>
                                                    <input type="text" class="ggg" name="pnrnumber" id="pnrnumber" placeholder="Enter PNR Number">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="portal">Portal</label>
                                                    <input type="text" class="ggg" name="portal" id="portal" placeholder="Enter Portal">
                                                </div>
                                                <!-- End Flight Data -->
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="train_sub">
                                                <!-- Train Data -->
                                                <div class="form-group col-sm-6">
                                                    <label for="t_jtype">Journey Type</label>
                                                    <select class="ggg" name="t_jtype" id="t_jtype">
                                                        <option value="">Select Journey Type</option>
                                                        <?php if($tjtype->num_rows() > 0){
                                                            foreach ($tjtype->result_array() as $tres) { ?>
                                                                <option value="<?php echo $tres['id'];?>"><?php echo $tres['type_name'];?></option>        
                                                            <?php }
                                                        }?>
                                                    </select>
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="t_doj">Date of Journey</label>
                                                    <input type="text" class="ggg dateofjourney" name="t_doj" id="t_doj" data-provide="datepicker" data-date-format="yyyy-mm-dd" placeholder="YYYY-MM-DD">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="t_source">Source</label>
                                                    <input type="text" class="ggg" name="t_source" id="t_source" placeholder="Enter Train Source">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="t_destination">Destination</label>
                                                    <input type="text" class="ggg" name="t_destination" id="t_destination" placeholder="Enter Train Destination">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="t_clstype">Class Type</label>
                                                    <select class="ggg" name="t_clstype" id="t_clstype">
                                                        <option value="">Select Class Type</option>
                                                        <?php if($tctype->num_rows() > 0){
                                                            foreach ($tctype->result_array() as $tct) { ?>
                                                                <option value="<?php echo $tct['id'];?>"><?php echo $tct['type_name'];?></option>
                                                            <?php }
                                                        }?>
                                                    </select>
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="t_noofpax">No of Pax</label>
                                                    <input type="text" class="ggg" name="t_noofpax" id="t_noofpax" placeholder="Enter No of Pax">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="t_pnrnumber">PNR Number</label>
                                                    <input type="text" class="ggg" name="t_pnrnumber" id="t_pnrnumber" placeholder="Enter PNR Number">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="t_portal">Portal</label>
                                                    <input type="text" class="ggg" name="t_portal" id="t_portal" placeholder="Enter Portal">
                                                </div>
                                                <!-- End Train Data -->
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="hotels_sub">
                                                <!-- Hotal Data -->
                                                <div class="form-group col-sm-6">
                                                    <label for="hotelname">Hotel Name</label>
                                                    <input type="text" class="ggg" name="hotelname" id="hotelname" placeholder="Enter Hotel Name">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="city">Location</label>
                                                    <input type="text" class="ggg" name="city" id="city" placeholder="Enter Location">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="checkin">Check In</label>
                                                    <input type="text" class="ggg dateofjourney" name="checkin" id="checkin" data-provide="datepicker" data-date-format="yyyy-mm-dd" placeholder="YYYY-MM-DD">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="checkout">Check Out</label>
                                                    <input type="text" class="ggg dateofjourney" name="checkout" id="checkout" data-provide="datepicker" data-date-format="yyyy-mm-dd" placeholder="YYYY-MM-DD">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="conformnumber">Conform Number</label>
                                                    <input type="text" class="ggg" name="conformnumber" id="conformnumber" placeholder="Enter Conform Number">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="h_portal">Portal</label>
                                                    <input type="text" class="ggg" name="h_portal" idh_portal" placeholder="Enter Portal">
                                                </div>
                                                <!-- End Hotal Data -->
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="cab_sub">
                                                <!-- Cab Data -->
                                                <div class="form-group col-sm-6">
                                                    <label for="c_doj">Date of Journey</label>
                                                    <input type="text" class="ggg dateofjourney" name="c_doj" id="c_doj" data-provide="datepicker" data-date-format="yyyy-mm-dd" placeholder="YYYY-MM-DD">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="c_source">Source</label>
                                                    <input type="text" class="ggg" name="c_source" id="c_source" placeholder="Enter Cab Source">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="c_destination">Destination</label>
                                                    <input type="text" class="ggg" name="c_destination" id="c_destination" placeholder="Enter Cab Destination">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="c_noofpax">No of Pax</label>
                                                    <input type="text" class="ggg" name="c_noofpax" id="c_noofpax" placeholder="Enter No of Pax">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="c_portal">Portal</label>
                                                    <input type="text" class="ggg" name="c_portal" id="c_portal" placeholder="Enter Portal">
                                                </div>
                                                <!-- End Cab Data -->
                                            </div>
                                        </div>
                                </div> 
                                <div class="col-sm-12">
                                    <fieldset>
                                        <legend>No of Users</legend>
                                        <div class="col-sm-12">
                                            <div class="row">
                                                <div class="form-group col-sm-3">
                                                    <input type="text" class="ggg" name="subcust[]" id="subcust" placeholder="Enter Customer Name">
                                                </div>
                                                <div class="form-group col-sm-3">
                                                    <input type="text" class="ggg" name="sub_mobile[]" id="sub_mobile" placeholder="Enter Mobile Number">
                                                </div>
                                                <div class="form-group col-sm-3">
                                                    <input type="text" class="ggg" name="sub_address[]" id="sub_address" placeholder="Enter Address">
                                                </div>
                                                <div class="form-group col-sm-3">
                                                    <span class="btn btn-primary addnewcust"><i class="fa fa-plus-square"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="addcust"></div>
                                    </fieldset>
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="bookingfare">Booking Fare</label>
                                    <input type="text" class="ggg amtchange" name="bookingfare" id="bookingfare" placeholder="Enter Booking Fare" value="0">
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="comission">Comission</label>
                                    <input type="text" class="ggg amtchange" name="comission" id="comission" placeholder="Enter Comission" value="0">
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="totalamount">Total Amount</label>
                                    <input type="text" class="ggg" name="totalamount" id="totalamount" placeholder="Enter Total Amount" value="0">
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="referal">Referal</label>
                                    <input type="text" class="ggg" name="referal" id="referal" placeholder="Enter Referal">
                                </div>
                                <div class="form-group col-sm-6" style="margin-top: 1px;">
                                    <label for="payment_type" >Payment Type</label>
                                    <select class="ggg" name="payment_type" id="payment_type">
                                        <option value="0">Cash</option>
                                        <option value="1">Cheque</option>
                                        <option value="2">Net Banking</option>
                                    </select>
                                </div>
                                <div class="form-group col-sm-6" id="pay_cheque" style="display: none;">
                                    <label for="cheque_number">Cheque Number</label>
                                    <input type="text" class="ggg" name="cheque_number" id="cheque_number" placeholder="Enter Cheque Number">
                                </div>
                                <div class="form-group col-sm-6" id="bank_ref_num" style="display: none;">
                                    <label for="bank_ref_num">Bank Reference Number</label>
                                    <input type="text" class="ggg" name="bank_ref_num" id="bank_ref_num" placeholder="Enter Bank Reference Number">
                                </div>
                                <div class="form-group col-sm-6" id="pay_amount">
                                    <label for="amount">Pay Amount</label>
                                    <input type="text" class="ggg" name="amount" id="amount" placeholder="Enter Pay Amount" value="0">
                                </div>
                                <div class="form-group col-sm-6" id="bank_ref_num">
                                    <label for="payment_date">Payment Date</label>
                                    <input type="text" class="ggg datepicker" name="payment_date" id="payment_date" data-provide="datepicker" data-date-format="yyyy-mm-dd" placeholder="YYYY-MM-DD">
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="amt_status">Amount Status</label>
                                    <select class="ggg" name="amt_status" id="amt_status">
                                        <option value="0">Successfully Paid</option>
                                        <option value="1">Pending</option>
                                    </select>
                                </div>
                                <div class="col-sm-12">
                                    <input type="submit" name="regform" class="btn btn-info" value="Create Booking">
                                </div>
                            </form>
                        </div>
                    </section>
                </div>
            </div>
        <!-- </div> -->
    </section>
</section>
<script type="text/javascript">
    $(document).ready(function(){
        $("form").submit(function(){
            if ($('input:checkbox').filter(':checked').length < 1){
                alert("Please cehck at least one Booking");
                return false;
            }
            /*if($('#cust_name').val() == ''){
                $('#formerrors').attr("class", "alert alert-danger");
                $('#formerrors').html('Please enter Customer Name');
                $('#cust_name').focus();
            }*/
        });
    });
</script>
<script type="text/javascript">
  $(document).ready(function() {
      $('#booking').DataTable( {
          dom: 'Bfrtip',
          buttons: [
              'copy', 'csv', 'excel', 'pdf', 'print'
          ]
      } );
  } );
</script>
<section id="main-content">
	<section class="wrapper">
		<div class="table-agile-info rem-pad">
		<!-- 728x90 -->
  <div class="panel panel-default">
    <?php
        if($this->session->flashdata('success_msg'))    
            {           
        echo "<div class='alert alert-success'>".$this->session->flashdata('success_msg')."</div>"; 
        }   
        if($this->session->flashdata('error_msg'))
            {       
        echo "<div class='alert alert-danger'>".$this->session->flashdata('error_msg')."</div>";    
        }           
    ?>
    <div class="panel-heading">
      Booking History
      <a href="<?=base_url()?>bookingactions/create" class="btn btn-info btn-xs" >Book Now</a>
    </div>
    <div class="table-responsive ">
      <table id="booking" class="table table-striped table-bordered display nowrap" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>Sno</th>
            <th>Customer Name</th>
            <th>Date of booking</th>
            <th>Traveling Mode</th>
            <th>Booking Fare</th>
            <th>Comission</th>
            <th>Total Amount</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php if($bookingres->num_rows() > 0) {
              $i = 1;
              foreach ($bookingres->result_array() as $res) { ?>
                <tr>
                  <td><a href="<?=base_url()?>bookingactions/show/<?php echo $res['id'];?>" class="btn btn-xs btn-info"><?php echo $i; ?></a></td>
                  <td><?php echo $res['cname']; ?></td>
                  <td><?php echo date('d M,Y',strtotime($res['dob'])); ?></td>
                  <td><?php echo getModetypes($res['mode_type']); ?></td>
                  <td><?php echo $res['booking_fare']; ?></td>
                  <td><?php echo $res['comission']; ?></td>
                  <td><?php echo $res['total_amount']; ?></td>
                  <td>
                    <a href="#" ui-toggle-class=""><i class="fa fa-check text-success text-active"></i><i class="fa fa-times text-danger text"></i></a>
                  </td>
                </tr>
            <?php  $i++; }
          }?>
        </tbody>
      </table>
    </div>
  </div>
  <!-- 728x90 -->
</div>
</section>
<section id="main-content">
	<section class="wrapper">
		<fieldset>
		    <legend>Booking Details :</legend>
		    <div class="col-sm-4">
		    	<div class="form-group">
			    	<label>Customer Name :</label> <?php echo $bookdata[0]['customer_name'];?>
			    </div>
		    </div>
		    <div class="col-sm-4">
		    	<div class="form-group">
			    	<label>Date of booking :</label> <?php echo date('d M,Y',strtotime($bookdata[0]['dob']));?>
			    </div>
		    </div>
		    <div class="col-sm-4">
		    	<div class="form-group">
			    	<label>Traveling Mode :</label> <?php echo getModetypes($bookdata[0]['mode_type']); ?>
			    </div>
		    </div>
		    <div class="col-sm-4">
		    	<div class="form-group">
			    	<label>Booking Fare :</label> <?php echo $bookdata[0]['booking_fare'];?>
			    </div>
		    </div>
		    <div class="col-sm-4">
		    	<div class="form-group">
			    	<label>Comission :</label> <?php echo $bookdata[0]['comission'];?>
			    </div>
		    </div>
		    <div class="col-sm-4">
		    	<div class="form-group">
			    	<label>Total Amount :</label> <?php echo $bookdata[0]['total_amount'];?>
			    </div>
		    </div>
		    <div class="col-sm-4">
		    	<div class="form-group">
			    	<label>Refered By :</label> <?php echo $bookdata[0]['referal'];?>
			    </div>
		    </div>
		</fieldset>
		<fieldset>
		    <legend>Transaction Details :</legend>
		    <table class="table table-responsive">
    			<thead>
    				<tr>
    					<th>Sno</th>
    					<th>Payment Mode</th>
    					<th>Amount Pay Date</th>
    					<th>Reference Number</th>
    					<th>Amount Status</th>
    					<th>Amount</th>
    				</tr>
    			</thead>
    			<tbody>
				    <?php if(count($bookdata) > 0){
				    		for ($i=0; $i < count($bookdata) ; $i++) { ?> 
				    		 	<tr>
			    					<td><?php echo $i+1;?></td>
			    					<td><?php if($bookdata[$i]['pament_mode'] == 0) echo "Cash"; else echo "Cheque"; ?></td>
			    					<td><?php echo date('d M,Y',strtotime($bookdata[$i]['pay_date']));?></td>
			    					<td><?php if(!empty($bookdata[$i]['reference_number'])) echo $bookdata[$i]['reference_number']; else echo "N/A"; ?></td>
			    					<td><?php if($bookdata[$i]['amount_status'] == 0) echo '<span class="label label-success"> Paid </span>'; else echo '<span class="label label-warning"> Pending </span>'; ?></td>
			    					<td><?php echo $bookdata[$i]['amount']; ?></td>
			    				</tr>
				    		<?php } 
						  }else{ ?>
				    	<tr>
				    		<td colspan="4">No Records found..</td>
				    	</tr>
					<?php }?>
						
				</tbody>
			</table>
		</fieldset>
		<fieldset>
		    <legend>User Details :</legend>
		    <table class="table table-responsive">
    			<thead>
    				<tr>
    					<th>Sno</th>
    					<th>User Name</th>
    					<th>Mobile Number</th>
    					<th>Age</th>
    					<th>Address</th>
    					<th>Status</th>
    				</tr>
    			</thead>
    			<tbody>
				    <?php if($busers->num_rows() > 0 ){
				    		$i=1;
				    	foreach ($busers->result_array() as $user) { ?>
				    	<tr>
				    		<td><?php echo $i;?></td>	
				    		<td><?php echo $user['sub_cust_name'];?></td>	
				    		<td><?php echo $user['mobile_nuber'];?></td>	
				    		<td><?php echo $user['age'];?></td>	
				    		<td><?php echo $user['address'];?></td>	
				    		<td><?php if($user['status'] == 1) echo "Active"; else echo "Inactive";?></td>	
				    	</tr>
				    	<?php $i++;	}
				    }?>
				</tbody>
			</table>
		</fieldset>
	</section>
</section>
<section id="main-content">
	<section class="wrapper">
    	<div class="form-w3layouts">
            <div class="row">
                <div class="col-lg-12">
                    <div id="formerrors"></div>
                    <?php echo validation_errors();?>
                    <?php
                        if($this->session->flashdata('success_msg'))    
                            {           
                        echo "<div class='alert alert-success'>".$this->session->flashdata('success_msg')."</div>"; 
                        }   
                        if($this->session->flashdata('error_msg'))
                            {       
                        echo "<div class='alert alert-danger'>".$this->session->flashdata('error_msg')."</div>";    
                        }           
                    ?>
                    <section class="panel">
                        <header class="panel-heading">
                            Booking Form
                        </header>
                        <div class="panel-body">
                            <form role="form" action="<?=base_url()?>bookingactions/store" method="post" name="bookingform" id="bookingform" >
                                <div class="form-group col-sm-6">
                                    <label for="cust_name">Customer Name</label>
                                    <input type="text" class="form-control" name="cust_name" id="cust_name" placeholder="Enter Customer Name" value="<?php echo $cust_name;?>">
                                    <input type="hidden" name="cust_id" id="cust_id" value="<?php echo $cust_id;?>">
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="dob">DOB (Date of Booking)</label>
                                    <input type="text" class="form-control datepicker" name="dob" id="dob" data-provide="datepicker" data-date-format="yyyy-mm-dd" placeholder="Enter Date of Booking">
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="doj">DOJ (Date of Journey)</label>
                                    <input type="text" class="form-control dateofjourney" name="doj" id="doj" data-provide="datepicker" data-date-format="yyyy-mm-dd" placeholder="Enter Date of Journey">
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="noofpax">No of Pax</label>
                                    <input type="text" class="form-control" name="noofpax" id="noofpax" placeholder="Enter Date of No of Pax">
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="from">From (Source)</label>
                                    <input type="text" class="form-control" name="from" id="from" placeholder="Enter From">
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="to">To (Destination)</label>
                                    <input type="text" class="form-control" name="to" id="to" placeholder="Enter To">
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="travelmod">Travel Mode</label>
                                    <select class="form-control" name="travelmod" id="travelmod">
                                        <option value="">Select One</option>
                                        <option value="Cab">Cab</option>
                                        <option value="flight">Flight</option>
                                        <option value="bus">Bus</option>
                                    </select>
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="travelname">Travel Name</label>
                                    <input type="text" class="form-control" name="travelname" id="travelname" placeholder="Enter Travel Name">
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="classtype">Class Type</label>
                                    <input type="text" class="form-control" name="classtype" id="classtype" placeholder="Enter Class Type">
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="pnrnumber">PNR Number</label>
                                    <input type="text" class="form-control" name="pnrnumber" id="pnrnumber" placeholder="Enter Travel Name">
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="bookingfare">Booking Fare</label>
                                    <input type="text" class="form-control amtchange" name="bookingfare" id="bookingfare" placeholder="Enter Booking Fare" value="0">
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="comission">Comission</label>
                                    <input type="text" class="form-control amtchange" name="comission" id="comission" placeholder="Enter Comission" value="0">
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="totalamount">Total Amount</label>
                                    <input type="text" class="form-control" name="totalamount" id="totalamount" placeholder="Enter Total Amount" value="0">
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="referal">Referal</label>
                                    <input type="text" class="form-control" name="referal" id="referal" placeholder="Enter Referal">
                                </div>
                                <div class="col-sm-12">
                                    <input type="submit" name="regform" class="btn btn-info" value="Create Booking">
                                </div>
                            </form>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>
</section>
<script type="text/javascript">
    $(document).ready(function(){
        $('.amtchange').on('change',function(){
            var amt = $('#bookingfare').val();
            var com = $('#comission').val();
            var tot = parseInt(amt)+parseInt(com);
            $("#totalamount").val(tot);
        });
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd'
        });
        $('.dateofjourney').datepicker({
            format: 'yyyy-mm-dd'
        });
    });
</script>
<script type="text/javascript">
  $(document).ready(function() {
      $('#tflights').DataTable( {
          dom: 'Bfrtip',
          buttons: [
              'copy', 'csv', 'excel', 'pdf', 'print'
          ]
      } );
  } );
</script>
<section id="main-content">
	<section class="wrapper">
		<div class="table-agile-info rem-pad">
			<div class="panel panel-default">
			    <?php
			        if($this->session->flashdata('success_msg'))    
			            {           
			        echo "<div class='alert alert-success'>".$this->session->flashdata('success_msg')."</div>"; 
			        }   
			        if($this->session->flashdata('error_msg'))
			            {       
			        echo "<div class='alert alert-danger'>".$this->session->flashdata('error_msg')."</div>";    
			        }           
			    ?>
			    <div class="panel-heading">
			      Flights
			    </div>
			    <div class="table-responsive">
				    <table id="tflights" class="table table-striped table-bordered display nowrap" cellspacing="0" width="100%">
				        <thead>
				          <tr>
				            <th>Sno</th>
				            <th>Customer Name</th>
				            <th>Journey Date</th>
				            <th>Souce</th>
				            <th>Destination</th>
				            <th>Flight Name</th>
				            <th>PNR Number</th>
				            <th>Portal</th>
				            <th>Status</th>
				          </tr>
				        </thead>
				        <tbody>
				          <?php if($flights->num_rows() > 0){
				          	$i=1;
				          	foreach ($flights->result_array() as $fres) { ?>
				          		<tr>
				          			<td><a href="<?=base_url()?>travelmodes/showflights/<?php echo $fres['id'];?>" class="btn btn-xs btn-primary"><?php echo $i;?></a></td>
				          			<td><?php echo $fres['customer_name'];?></td>
				          			<td><?php echo date('d M,Y',strtotime($fres['date_of_journey']));?></td>
				          			<td><?php echo $fres['source'];?></td>
				          			<td><?php echo $fres['destination'];?></td>
				          			<td><?php echo $fres['flight_name'];?></td>
				          			<td><?php echo $fres['pnr_number'];?></td>
				          			<td><?php echo $fres['portal'];?></td>
				          			<td><?php if($fres['status']) echo "Active"; else echo "Inactive";?></td>
				          		</tr>
				          	<?php $i++; }
				          }else{ ?>
				          	<tr>
				          		<td colspan="9">No Data Found...</td>
				          	</tr>
				          <?php	}?>
				        </tbody>
				    </table>
				</div>
			</div>
		</div>
	</section>
</section>

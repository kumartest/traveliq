<section id="main-content">
	<section class="wrapper">
            <div class="row">
                <div class="col-lg-12 butn">
                    <div id="formerrors"></div>
                    <?php echo validation_errors();?>
                    <?php
                        if($this->session->flashdata('success_msg'))    
                            {           
                        echo "<div class='alert alert-success'>".$this->session->flashdata('success_msg')."</div>"; 
                        }   
                        if($this->session->flashdata('error_msg'))
                            {       
                        echo "<div class='alert alert-danger'>".$this->session->flashdata('error_msg')."</div>";    
                        }           
                    ?>
                    <section >
                        <header class="panel-heading">
                            Customer Registration
                        </header>
                        <div class="panel-body">
                            <form role="form" action="<?=base_url()?>customer/insertcustomer" method="post" name="userform" id="customerform" >
                                <div class="form-group col-sm-6">
                                    <label for="custname">Customer Name</label>
                                    <input type="text" class="ggg" name="custname" id="custname" placeholder="Enter Customer Name">
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="moblienumber">Mobile Number</label>
                                    <input type="text" class="ggg" name="moblienumber" id="moblienumber" placeholder="Enter Mobile Number">
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="email">Email</label>
                                    <input type="email" class="ggg" name="email" id="email" placeholder="Enter Email">
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="address">Address</label>
                                    <textarea class="ggg" name="address" id="address" placeholder="Enter Address"></textarea>
                                </div>
                                <div class="col-sm-12">
                                    <input type="submit" name="regform" class="btn btn-info" value="Create User">
                                </div>
                            </form>
                        </div>
                    </section>
                </div>
            </div>
    </section>
</section>
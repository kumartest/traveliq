<script type="text/javascript">
  $(document).ready(function() {
      $('#customer').DataTable( {
          dom: 'Bfrtip',
          buttons: [
              'copy', 'csv', 'excel', 'pdf', 'print'
          ]
      } );
  } );
</script>
<section id="main-content">
	<section class="wrapper">
		<div class="table-agile-info rem-pad">
		<!-- 728x90 -->
  <div class="panel panel-default">
    <?php
        if($this->session->flashdata('success_msg'))    
            {           
        echo "<div class='alert alert-success'>".$this->session->flashdata('success_msg')."</div>"; 
        }   
        if($this->session->flashdata('error_msg'))
            {       
        echo "<div class='alert alert-danger'>".$this->session->flashdata('error_msg')."</div>";    
        }           
    ?>
    <div class="panel-heading">
      Users
      <a href="<?=base_url()?>customer/create" class="btn btn-info btn-xs" >Create User</a>
    </div>
    <div class="table-responsive">
      <table id="customer" class="table table-striped table-bordered display nowrap" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>Sno</th>
            <th>Customer Name</th>
            <th>Mobile</th>
            <th>Email</th>
            <th>Address</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php if($customers->num_rows() > 0) {
              $i=1;
            foreach ($customers->result_array() as $res) { ?>
              <tr>
                  <td><?php echo $i; ?></td>
                  <td><?php echo $res['customer_name']; ?></td>
                  <td><?php echo $res['mobile_number']; ?></td>
                  <td><?php echo $res['email']; ?></td>
                  <td><?php echo $res['address']; ?></td>
                  <td><a href="<?=base_url()?>customer/editcustomer/<?php echo $res['id'];?>" class="btn btn-xs btn-primary"><i class="fa fa-pencil" title="Edit"></i></a><!-- &nbsp;<a href="<?=base_url()?>bookingactions/create/<?php echo $res['id'];?>" class="btn btn-xs btn-info">Book Now</a> --></td>
              </tr>
          <?php $i++;  }
          }?>
        </tbody>
      </table>
    </div>
  </div>
  <!-- 728x90 -->
</div>
</section>
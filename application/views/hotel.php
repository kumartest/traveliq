<script type="text/javascript">
  $(document).ready(function() {
      $('#thotels').DataTable( {
          dom: 'Bfrtip',
          buttons: [
              'copy', 'csv', 'excel', 'pdf', 'print'
          ]
      } );
  } );
</script>
<section id="main-content">
	<section class="wrapper">
		<div class="table-agile-info rem-pad">
			<div class="panel panel-default">
			    <?php
			        if($this->session->flashdata('success_msg'))    
			            {           
			        echo "<div class='alert alert-success'>".$this->session->flashdata('success_msg')."</div>"; 
			        }   
			        if($this->session->flashdata('error_msg'))
			            {       
			        echo "<div class='alert alert-danger'>".$this->session->flashdata('error_msg')."</div>";    
			        }           
			    ?>
			    <div class="panel-heading">
			      Hotels
			    </div>
			    <div class="table-responsive">
				    <table id="thotels" class="table table-striped table-bordered display nowrap" cellspacing="0" width="100%">
				        <thead>
				          <tr>
				            <th>Sno</th>
				            <th>Customer Name</th>
				            <th>Hotel Name</th>
				            <th>Location</th>
				            <th>Check In</th>
				            <th>Check Out</th>
				            <th>Confirm Number</th>
				            <th>Portal</th>
				            <th>Status</th>
				          </tr>
				        </thead>
				        <tbody>
				          <?php if($hotels->num_rows() > 0){
				          	$i=1;
				          	foreach ($hotels->result_array() as $hres) { ?>
				          		<tr>
				          			<td><a href="<?=base_url()?>travelmodes/showhotel/<?php echo $hres['id'];?>" class="btn btn-xs btn-primary"><?php echo $i;?></a></td>
				          			<td><?php echo $hres['customer_name'];?></td>
				          			<td><?php echo $hres['hotel_name'];?></td>
				          			<td><?php echo $hres['location'];?></td>
				          			<td><?php echo $hres['check_in'];?></td>
				          			<td><?php echo $hres['check_out'];?></td>
				          			<td><?php echo $hres['confirm_number'];?></td>
				          			<td><?php echo $hres['portal'];?></td>
				          			<td><?php if($hres['status']) echo "Active"; else echo "Inactive";?></td>
				          		</tr>
				          	<?php $i++; }
				          }else{ ?>
				          	<tr>
				          		<td colspan="9" class="txt-center">No Data Found...</td>
				          	</tr>
				          <?php	}?>
				        </tbody>
				    </table>
				</div>
			</div>
		</div>
	</section>
</section>

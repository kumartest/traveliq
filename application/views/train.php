<script type="text/javascript">
  $(document).ready(function() {
      $('#ttrains').DataTable( {
          dom: 'Bfrtip',
          buttons: [
              'copy', 'csv', 'excel', 'pdf', 'print'
          ]
      } );
  } );
</script>
<section id="main-content">
	<section class="wrapper">
		<div class="table-agile-info rem-pad">
			<div class="panel panel-default">
			    <?php
			        if($this->session->flashdata('success_msg'))    
			            {           
			        echo "<div class='alert alert-success'>".$this->session->flashdata('success_msg')."</div>"; 
			        }   
			        if($this->session->flashdata('error_msg'))
			            {       
			        echo "<div class='alert alert-danger'>".$this->session->flashdata('error_msg')."</div>";    
			        }           
			    ?>
			    <div class="panel-heading">
			      Trains
			    </div>
			    <div class="table-responsive">
				    <table id="ttrains" class="table table-striped table-bordered display nowrap" cellspacing="0" width="100%">
				        <thead>
				          <tr>
				            <th>Sno</th>
				            <th>Customer Name</th>
				            <th>Journey Date</th>
				            <th>Souce</th>
				            <th>Destination</th>
				            <th>Class Type</th>
				            <th>No.of Passingers</th>
				            <th>PNR Number</th>
				            <th>Portal</th>
				            <th>Status</th>
				          </tr>
				        </thead>
				        <tbody>
				          <?php if($trains->num_rows() > 0){
				          	$i=1;
				          	foreach ($trains->result_array() as $tres) { ?>
				          		<tr>
				          			<td><a href="<?=base_url()?>travelmodes/showtrain/<?php echo $tres['id'];?>" class="btn btn-xs btn-primary"><?php echo $i;?></a></td>
				          			<td><?php echo $tres['customer_name'];?></td>
				          			<td><?php echo $tres['date_of_journey'];?></td>
				          			<td><?php echo $tres['source'];?></td>
				          			<td><?php echo $tres['destination'];?></td>
				          			<td><?php echo $tres['class_type'];?></td>
				          			<td><?php echo $tres['no_of_pax'];?></td>
				          			<td><?php echo $tres['pnr_number'];?></td>
				          			<td><?php echo $tres['portal'];?></td>
				          			<td><?php if($tres['status']) echo "Active"; else echo "Inactive";?></td>
				          		</tr>
				          	<?php $i++; }
				          }else{ ?>
				          	<tr>
				          		<td colspan="9">No Data Found...</td>
				          	</tr>
				          <?php	}?>
				        </tbody>
				    </table>
				</div>
			</div>
		</div>
	</section>
</section>

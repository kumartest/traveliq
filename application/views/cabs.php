<script type="text/javascript">
  $(document).ready(function() {
      $('#tcabs').DataTable( {
          dom: 'Bfrtip',
          buttons: [
              'copy', 'csv', 'excel', 'pdf', 'print'
          ]
      } );
  } );
</script>
<section id="main-content">
	<section class="wrapper">
		<div class="table-agile-info rem-pad">
			<div class="panel panel-default">
			    <?php
			        if($this->session->flashdata('success_msg'))    
			            {           
			        echo "<div class='alert alert-success'>".$this->session->flashdata('success_msg')."</div>"; 
			        }   
			        if($this->session->flashdata('error_msg'))
			            {       
			        echo "<div class='alert alert-danger'>".$this->session->flashdata('error_msg')."</div>";    
			        }           
			    ?>
			    <div class="panel-heading">
			      Cabs
			    </div>
			    <div class="table-responsive">
				    <table id="tcabs" class="table table-striped table-bordered display nowrap" cellspacing="0" width="100%">
				        <thead>
				          <tr>
				            <th>Sno</th>
				            <th>Customer Name</th>
				            <th>Journey Date</th>
				            <th>Souce</th>
				            <th>Destination</th>
				            <th>No Of Passingers</th>
				            <th>Portal</th>
				            <th>Status</th>
				          </tr>
				        </thead>
				        <tbody>
				          <?php if($cabs->num_rows() > 0){
				          	$i=1;
				          	foreach ($cabs->result_array() as $cres) { ?>
				          		<tr>
				          			<td><a href="<?=base_url()?>travelmodes/showcab/<?php echo $cres['id'];?>" class="btn btn-xs btn-primary"><?php echo $i;?></a></td>
				          			<td><?php echo $cres['customer_name'];?></td>
				          			<td><?php echo date('d M,Y',strtotime($cres['date_of_journey']));?></td>
				          			<td><?php echo $cres['source'];?></td>
				          			<td><?php echo $cres['destination'];?></td>
				          			<td><?php echo $cres['no_of_pax'];?></td>
				          			<td><?php echo $cres['portal'];?></td>
				          			<td><?php if($cres['status']) echo "Active"; else echo "Inactive";?></td>
				          		</tr>
				          	<?php $i++; }
				          }else{ ?>
				          	<tr>
				          		<td colspan="9">No Data Found...</td>
				          	</tr>
				          <?php	}?>
				        </tbody>
				    </table>
				</div>
			</div>
		</div>
	</section>
</section>

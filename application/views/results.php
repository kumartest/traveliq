<section id="main-content">
	<section class="wrapper">
		<div class="row">
            <div class="col-lg-12 butn">
            	<div id="error_msg"></div>
            	<form role="form" action="" method="post" name="userform" id="customerform" >
                    <div class="form-group col-sm-6">
                        <label for="book_res">Customer Names</label>
                        <select class="ggg book_res" name="book_res" id="book_res">
                        	<option value="">Select</option>
                        	<?php if($customers->num_rows() > 0){
                        		foreach ($customers->result_array() as $res) { ?>
                        			<option value="<?php echo $res['id'];?>"><?php echo $res['customer_name'];?></option>
                        		<?php }
                        	} ?>
                        </select>
                    </div>
                </form>
                <div class="col-lg-12">
	                <table class="table table-responsive">
	                	<thead>
	                		<tr>
	                			<th>Sno</th>
	                			<th>Customer Name</th>
	                			<th>Date of booking</th>
	                			<th>Mode type</th>
	                			<th>Payment Type</th>
	                			<th>Reference Number</th>
	                			<th>Status</th>
	                			<th>Amount</th>
	                			<th>Actions</th>
	                		</tr>
	                	</thead>
	                	<tbody id="viewdata">
	                		
	                	</tbody>
	                </table>
                </div>
            </div>
        </div>
	</section>
</section>
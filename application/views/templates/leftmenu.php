<!-- sidebar start-->
<aside>
    <div id="sidebar" class="nav-collapse">
        <!-- sidebar menu start-->
        <div class="leftside-navigation">
            <ul class="sidebar-menu" id="nav-accordion">
            <?php 
                $selmethods1 = array("dashboard");
                if(in_array($this->router->fetch_method(),$selmethods1)){
                    $clsactive1 = "active";
                }else if(in_array($this->router->fetch_class(),$selmethods1)){
                    $clsactive1 = "active";
                }else{
                    $clsactive1 = "";
                }
            ?>
                <li>
                    <a class="<?php echo $clsactive1; ?>" href="<?=base_url();?>dashboard">
                        <i class="fa fa-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
            <?php 
                $selmethods1 = array("bookingactions");
                if(in_array($this->router->fetch_method(),$selmethods1)){
                    $clsactive2 = "active";
                }else if(in_array($this->router->fetch_class(),$selmethods1)){
                    $clsactive2 = "active";
                }else{
                    $clsactive2 = "";
                }
            ?>
                <li>
                    <a class="<?php echo $clsactive2; ?>" href="<?=base_url();?>bookingactions">
                        <i class="fa fa-book"></i>
                        <span>Booking Hisory</span>
                    </a>
                </li>
            <!-- Customers -->
            <?php 
                $selmethods1 = array("customer");
                if(in_array($this->router->fetch_method(),$selmethods1)){
                    $clsactive3 = "active";
                }else if(in_array($this->router->fetch_class(),$selmethods1)){
                    $clsactive3 = "active";
                }else{
                    $clsactive3 = "";
                }
            ?>
                <li>
                    <a class="<?php echo $clsactive3; ?>" href="<?=base_url();?>customer">
                        <i class="fa fa-users"></i>
                        <span>Customers</span>
                    </a>
                </li>
            <!-- End Customers -->

            <?php 
                $selmethods1 = array("flights","train","hotel","cab");
                if(in_array($this->router->fetch_method(),$selmethods1)){
                    $clsactive4 = "active";
                }else if(in_array($this->router->fetch_class(),$selmethods1)){
                    $clsactive4 = "active";
                }else{
                    $clsactive4 = "";
                }
            ?>  
                <li class="sub-menu">
                    <a class="<?php echo $clsactive4; ?>" href="javascript:;">
                        <i class="fa fa-gears"></i>
                        <span>Travel Modes</span>
                    </a>
                    <ul class="sub">
						<li><a <?php if($this->router->fetch_method() == "flights") echo 'class="active"';?> href="<?=base_url()?>travelmodes/flights">Flights</a></li>
                        <li><a <?php if($this->router->fetch_method() == "train") echo 'class="active"';?> href="<?=base_url()?>travelmodes/train">Trains</a></li>
                        <li><a <?php if($this->router->fetch_method() == "hotel") echo 'class="active"';?> href="<?=base_url()?>travelmodes/hotel">Hotel</a></li>
                        <li><a <?php if($this->router->fetch_method() == "cab") echo 'class="active"';?> href="<?=base_url()?>travelmodes/cab">Cab</a></li>
                    </ul>
                </li>
                <li>
                    <a <?php if($this->router->fetch_method() == "result") echo 'class="active"'; ?> href="<?=base_url();?>bookingactions/results">
                        <i class="fa fa-users"></i>
                        <span>Booking Results</span>
                    </a>
                </li>
                
                <!-- <li class="sub-menu">
                    <a href="javascript:;">
                        <i class="fa fa-tasks"></i>
                        <span>Form Components</span>
                    </a>
                    <ul class="sub">
                        <li><a href="form_component.html">Form Elements</a></li>
                        <li><a href="form_validation.html">Form Validation</a></li>
						<li><a href="dropzone.html">Dropzone</a></li>
                    </ul>
                </li> -->
                <!-- <li class="sub-menu">
                    <a href="javascript:;">
                        <i class="fa fa-envelope"></i>
                        <span>Mail </span>
                    </a>
                    <ul class="sub">
                        <li><a href="mail.html">Inbox</a></li>
                        <li><a href="mail_compose.html">Compose Mail</a></li>
                    </ul>
                </li>
                <li class="sub-menu">
                    <a href="javascript:;">
                        <i class=" fa fa-bar-chart-o"></i>
                        <span>Charts</span>
                    </a>
                    <ul class="sub">
                        <li><a href="chartjs.html">Chart js</a></li>
                        <li><a href="flot_chart.html">Flot Charts</a></li>
                    </ul>
                </li>
                <li class="sub-menu">
                    <a href="javascript:;">
                        <i class=" fa fa-bar-chart-o"></i>
                        <span>Maps</span>
                    </a>
                    <ul class="sub">
                        <li><a href="google_map.html">Google Map</a></li>
                        <li><a href="vector_map.html">Vector Map</a></li>
                    </ul>
                </li>
                <li class="sub-menu">
                    <a href="javascript:;">
                        <i class="fa fa-glass"></i>
                        <span>Extra</span>
                    </a>
                    <ul class="sub">
                        <li><a href="gallery.html">Gallery</a></li>
						<li><a href="404.html">404 Error</a></li>
                        <li><a href="registration.html">Registration</a></li>
                    </ul>
                </li> -->
            </ul>            
        </div>
        <!-- sidebar menu end-->
    </div>
</aside>
<!--sidebar end
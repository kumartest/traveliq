<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Travelmodes extends CI_Controller {

	public function __Construct(){
		parent::__Construct();
		is_logged_in();
		$this->load->model('Tmodes');
		$this->user = $this->session->userdata('user_id');
	}
	/* Flights Data */
	public function flights()
	{
		$uid = $this->user;
		$data['page_title'] = "Flights";
		$data['flights'] = $this->Tmodes->getAllTravelsData("tbl_flight_booking as t",$uid);
		$this->settemplate->dashboard("flights",$data);
	}

	public function showflights($id)
	{
		# code...
	}

	/* Train Data */
	public function train()
	{
		$uid = $this->user;
		$data['page_title'] = "Trains";
		$data['trains'] = $this->Tmodes->getAllTravelsData("tbl_train_booking as t",$uid);
		$this->settemplate->dashboard("train",$data);
	}

	public function showtrain($id)
	{
		# code...
	}


	/* Hotel Data */
	public function hotel()
	{
		$uid = $this->user;
		$data['page_title'] = "Hotels";
		$data['hotels'] = $this->Tmodes->getAllTravelsData("tbl_hotel_booking as t",$uid);
		$this->settemplate->dashboard("hotel",$data);
	}

	public function showhotel($id)
	{
		# code...
	}

	/* Cab Data */
	public function cab()
	{
		$uid = $this->user;
		$data['page_title'] = "Cabs";
		$data['cabs'] = $this->Tmodes->getAllTravelsData("tbl_cab_booking as t",$uid);
		$this->settemplate->dashboard("cabs",$data);
	}

	public function showcab($id)
	{
		# code...
	}
}
<?php 
//ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');
class Userlogin extends CI_Controller {
	public function __Construct()
	{
		parent::__Construct();
		$this->load->model('Traveliq');
	}

	public function index()
	{
		$data['title'] = "Login";
		if($this->session->userdata('t_loggedin')){
			redirect('dashboard');
		}
		$this->settemplate->login("",$data);
	}

	public function login()
	{
		$data["page_title"] = "Login";
		$this->form_validation->set_rules('usermobile','Mobile Number','trim|required');
		$this->form_validation->set_rules('userpwd','Password','trim|required');
		if ($this->form_validation->run() == FALSE)
		{	
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
			$this->index();
		}else{
			$umobile = $this->input->post('usermobile');
			$upwd = $this->input->post('userpwd');

			$whr = array('mobile_number' => $umobile,'password' => $upwd , 'status' => 1);
			$result = $this->Traveliq->checkUserLogin($whr,"tbl_users");

			if(count($result) > 0){
				$this->session->set_userdata('user_id',$result[0]->id);
				$this->session->set_userdata('user_name',$result[0]->user_name);
				$this->session->set_userdata('user_email',$result[0]->email_id);
				$this->session->set_userdata('user_moblie',$result[0]->mobile_number);
				$this->session->set_userdata('role',$result[0]->role);
				$this->session->set_userdata('t_loggedin',TRUE);
				redirect('dashboard');
			}else{
				$this->session->set_flashdata('error_msg',"Invalid Mobile Number/Password");
				redirect('userlogin');
			}
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		$this->session->unset_userdata('user_id');
		$this->session->unset_userdata('user_name');
		$this->session->unset_userdata('user_email');
		$this->session->unset_userdata('user_moblie');
		$this->session->unset_userdata('role');
		$this->session->unset_userdata('t_loggedin');
		redirect('/');
	}

}
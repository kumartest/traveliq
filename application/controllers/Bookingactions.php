<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bookingactions extends CI_Controller {

	public function __Construct(){
		parent::__Construct();
		is_logged_in();
		$this->load->model('Traveliq');
		$this->user = $this->session->userdata('user_id');
	}

	public function index()
	{
		$data['page_title'] = "Booking History";
		$user_id = $this->user;
		$data['bookingres'] = $this->Traveliq->getAllBookings("tbl_booking",$user_id);
		$this->settemplate->dashboard('booking/booking',$data);
	}

	public function create($id='NULL')
	{
		$data['page_title'] = "Booking History";
		$data['fjtype'] = $this->Traveliq->getJourneyType("tbl_journey_types",1);
		$data['tjtype'] = $this->Traveliq->getJourneyType("tbl_journey_types",2);
		$data['fctype'] = $this->Traveliq->getJourneyType("tbl_class_types",1);
		$data['tctype'] = $this->Traveliq->getJourneyType("tbl_class_types",2);
		$data['customer'] = $this->Traveliq->getCustomer("tbl_customers");
		$data['tmodes'] = $this->Traveliq->getTravelModes("tbl_travel_modes");
		$this->settemplate->dashboard('booking/create',$data);
	}

	public function store()
	{
		$user_id = $this->user;
		$date = date('Y-m-d H:i:s');
		$this->form_validation->set_rules('cust_name','Customer Name','trim|required');
		$this->form_validation->set_rules('dob','Date of booking','trim|required');
		$travelmodes = $this->input->post('tmode');
		if(count($travelmodes) > 0 ){
			$tmodes = implode(",",$travelmodes);
			$binput['customer_id'] = $this->input->post('cust_name');
			$binput['dob'] = $this->input->post('dob');
			$binput['booking_fare'] = $this->input->post('bookingfare');
			$binput['comission'] = $this->input->post('comission');
			$binput['total_amount'] = $this->input->post('totalamount');
			$binput['referal'] = $this->input->post('referal');
			// Create booking
			$bookId = $this->createBooking($user_id,$tmodes,$binput);
			
			for ($i=0; $i < count($travelmodes) ; $i++) {
				if($travelmodes[$i] == 1){ // Flights Data
					$time = $this->input->post('time1').'-'.$this->input->post('time2');
					$flight = array('book_id' => $bookId, 'cust_id' => $binput['customer_id'], 'type' => $this->input->post('ttype'), 'journey_type' => $this->input->post('journeytype'), 'date_of_journey' => $this->input->post('doj'), 'time' => $time, 'source' => $this->input->post('f_souce'), 'destination' => $this->input->post('f_destination'), 'class_type' => $this->input->post('clstype'), 'no_of_pax' => $this->input->post('noofpax'), 'flight_name' =>$this->input->post('flight_name') , 'pnr_number' => $this->input->post('pnrnumber'), 'portal' => $this->input->post('portal'), 'status' => 1,'user_id' => $user_id, 'create_at' => $date, 'updated_at' =>$date);
					$flightBookId = $this->Traveliq->insertData("tbl_flight_booking",$flight);
				}else if($travelmodes[$i] == 2){//Train Data
					$train = array('book_id' => $bookId, 'cust_id' => $binput['customer_id'],'journey_type' => $this->input->post('t_jtype'), 'date_of_journey' => $this->input->post('t_doj'), 'source' => $this->input->post('t_source'), 'destination' => $this->input->post('t_destination'), 'class_type' => $this->input->post('t_clstype'), 'no_of_pax' => $this->input->post('t_noofpax'), 'pnr_number' => $this->input->post('t_pnrnumber'), 'portal' =>$this->input->post('t_portal') , 'status' => 1,'user_id' => $user_id, 'created_at' =>$date , 'updated_at' =>$date);
					$trainBookingId = $this->Traveliq->insertData("tbl_train_booking",$train);
				}else if($travelmodes[$i] == 3){// Hotel Booking
					$hotel = array('book_id' => $bookId, 'cust_id' => $binput['customer_id'], 'hotel_name' => $this->input->post('hotelname'), 'location' => $this->input->post('city'), 'check_in' => $this->input->post('checkin'), 'check_out' => $this->input->post('checkout'), 'confirm_number' => $this->input->post('conformnumber'), 'portal' => $this->input->post('h_portal'), 'status' => 1,'user_id' => $user_id, 'created_at' => $date, 'updated_at' =>$date);
					$trainBookingId = $this->Traveliq->insertData("tbl_hotel_booking",$hotel);
				}else if($travelmodes[$i] == 4){// Cab Booking
					$cab = array('book_id' => $bookId, 'cust_id' => $binput['customer_id'], 'date_of_journey' => $this->input->post('c_doj'), 'source' => $this->input->post('c_source'), 'destination' => $this->input->post('c_destination'), 'no_of_pax' => $this->input->post('c_noofpax'), 'portal' => $this->input->post('c_portal'),'user_id' => $user_id, 'status' => 1, 'created_at' => $date, 'updated_at' =>$date);
					$trainBookingId = $this->Traveliq->insertData("tbl_cab_booking",$cab);
				}	
			} 
			// Insert Customers Data
			$cust_id = $this->insertCustomerData($bookId,$tmodes,$_POST);
			// Make a Amount Transaction
			$transction = $this->makeBookingTransaction($bookId,$_POST);
			// flashmsg
			redirect('bookingactions');
		}else{
			echo "Smothing Went Wrong!";
		}
		redirect('bookingactions');
	}

	public function createBooking($uid,$mode_types,$input)
	{
		$date = date('Y-m-d H:i:s');
		$booking = array('customer_id' => $input['customer_id'], 'dob' => $input['dob'], 'mode_type' => $mode_types, 'booking_fare' => $input['booking_fare'], 'comission' => $input['comission'], 'total_amount' => $input['total_amount'], 'referal' => $input['referal'], 'status' => 1, 'user_id' =>$uid , 'created_at' =>$date , 'updated_at' =>$date);
		$book_id = $this->Traveliq->insertData("tbl_booking",$booking);
		return $book_id;
	}

	public function insertCustomerData($bookid,$tmodes,$input)
	{
		$date = date('Y-m-d H:i:s');
		$customer = $input['subcust'];
		$s_mobile = $input['sub_mobile'];
		$s_address = $input['sub_address'];
		if(count($customer) > 0){
			for ($i=0; $i < count($customer); $i++) { 
				$cdata['book_id'] = $bookid; 
				$cdata['cust_id'] = $input['cust_name']; 
				//$cdata['mode_type'] = $mtype; 
				$cdata['sub_cust_name'] = $customer[$i]; 
				$cdata['mobile_nuber'] = $s_mobile[$i]; 
				$cdata['age'] = 0; 
				$cdata['address'] = $s_address[$i]; 
				$cdata['status'] = 1; 
				$cdata['created_at'] = $date; 
				$cdata['updated_at'] = $date; 
				$sub_customers = $this->Traveliq->insertData("tbl_sub_customers",$cdata);
			}
			return $sub_customers;
		}else{
			return "";
		}
	}

	public function makeBookingTransaction($book_id,$input)
	{
		$date = date('Y-m-d H:i:s');
		$uid = $this->user;
		$tinput['book_id'] = $book_id;
		$tinput['cust_id'] = $input['cust_name'];
		$tinput['pament_mode'] = $input['payment_type'];
		$tinput['account_type'] = 1;
		$tinput['amount'] = $input['totalamount'];
		$tinput['amount_status'] = $input['amt_status'];
		$tinput['pay_date'] = $input['payment_date'];
		$tinput['status'] = 1;
		$tinput['user_id'] = $uid;
		$tinput['created_at'] = $date;
		$tinput['updated_at'] = $date;
		if($input['payment_type'] == 0){//Cash
			if($input['amount'] > 0){
				$tinput['account_type'] = 1;
				$tinput['amount'] = $input['amount'];
				$txn_id = $this->Traveliq->insertData("tbl_transaction",$tinput);
			}
		}else if($input['payment_type'] == 1){//Cheque
			$tinput['account_type'] = 1;
			$tinput['amount_status'] = 1;
			$tinput['reference_number'] = $input['cheque_number'];
			$txn_id = $this->Traveliq->insertData("tbl_transaction",$tinput);
		}else{// Net Banking
			$tinput['account_type'] = 1;
			$tinput['amount_status'] = 0;
			$tinput['reference_number'] = $input['bank_ref_num'];
			$txn_id = $this->Traveliq->insertData("tbl_transaction",$tinput);
		}
		return $txn_id;
	}


	public function show($id)
	{
		$uid = $this->user;
		$data['page_title'] = "Show booking";
		$data['bookdata'] = $this->Traveliq->getBookingDetails($id);
		if(count($data['bookdata'])){
			$cid = $data['bookdata'][0]['customer_id'];
		}else{
			$cid=0; 
		}
		$data['busers'] = $this->Traveliq->getBookingUsers($id,$cid);
		$income = $this->Traveliq->getAmountDetailsById($id,1);
		$expance = $this->Traveliq->getAmountDetailsById($id,2);
		$data['balance'] = $expance-$income;
		$this->settemplate->dashboard("booking/show",$data);
	}

	public function results()
	{
		$data['page_title'] = "Booking Results";
		$data['customers'] = $this->Traveliq->getCustomersData("tbl_customers");
		$this->settemplate->dashboard("results",$data);
	}

	public function getbooking_results()
	{
		$uid = $this->user;
		$pending = 0; $paid = 0; $rejected = 0; $tot_amt = 0;
		$id = $_GET['id'];
		$data['result'] = $this->Traveliq->getBookingsById("tbl_booking as b",$id,$uid)->result_array();
		$get_amount = $this->Traveliq->getBookingAmount("tbl_transaction",$id,$uid);
		if($get_amount->num_rows() >0){
			foreach ($get_amount->result_array() as $amt) {
				$tot_amt += $amt['amount'];
				if($amt['amount_status'] == 0){
					$paid += $amt['amount'];
				}else if($amt['amount_status'] == 1) {
					$pending += $amt['amount'];
				}else{
					$rejected += $amt['amount'];
				}
			}
		}
		$data['amounts'] = array('tot_amount' => $tot_amt,'pending' => $pending,'paid' => $paid,'rejected' => $rejected);
		echo $this->load->view("travel_results",$data);
	}

	public function changeamountstatus()
	{
		$data = array();
		$uid = $this->user;
		$whr = array('id'=>$_GET['id'],'user_id' => $uid);
		$set = array('amount_status' =>$_GET['status']);
		$result = $this->db->set($set)->where($whr)->update("tbl_transaction");
		if($result > 0){
			$data['id'] = 1; 
		}else{
			$data['id'] = 0;
		}
		echo json_encode($data);
	}

	public function edit()
	{
		
	}

	public function update()
	{

	}

	public function delete()
	{
		
	}
}
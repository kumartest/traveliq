<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller {

	public function __Construct(){
		parent::__Construct();
		is_logged_in();
		$this->load->model('Traveliq');
	}

	public function index()
	{
		$data['page_title'] = "Users";
		$data['customers'] = $this->Traveliq->getCustomersData("tbl_customers");
		$this->settemplate->dashboard("customers/customer",$data);
	}

	public function create()
	{
		$data['page_title'] = "Create User";
		$this->settemplate->dashboard("customers/create",$data);
	}

	public function insertcustomer()
	{
		$date = date('Y-m-d H:i:s');
		$this->form_validation->set_rules('custname','Customer Name','trim|required');
		$this->form_validation->set_rules('moblienumber','Mobile Number','trim|required');
		if($this->form_validation->run() == false){
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
			$this->index();
		}else{
			$userdata = array('customer_name' => $this->input->post('custname'), 'mobile_number' => $this->input->post('moblienumber'), 'email' => $this->input->post('email'), 'address' => $this->input->post('address'), 'status' => 1, 'created_at' => $date, 'updated_at' => $date);
			$result = $this->Traveliq->insertData("tbl_customers",$userdata);
			if($result > 0 ){
				$this->session->set_flashdata('success_msg',"Added successfully!");
				redirect('customer');
			}else{
				$this->session->set_flashdata('error_msg',"Adding failed,please try again!");
			}
		}
		redirect('customer/create');	
	}

	public function editcustomer($id)
	{
		$data['page_title'] = "Edit Customer";
		$data['customer_details'] = $this->Traveliq->getCustomerDetails($id);
		$this->settemplate->dashboard('customers/edit',$data);
	}

	public function updatecustomer($id)
	{
		$date = date('Y-m-d H:i:s');
		$this->form_validation->set_rules('custname','Customer Name','trim|required');
		$this->form_validation->set_rules('moblienumber','Mobile Number','trim|required');
		if($this->form_validation->run() == false){
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
			$this->editcustomer($id);
		}else{
			$whr = array('id'=>$id);
			$userdata = array('customer_name' => $this->input->post('custname'), 'mobile_number' => $this->input->post('moblienumber'), 'email' => $this->input->post('email'), 'address' => $this->input->post('address'), 'updated_at' => $date);
			$result = $this->Traveliq->updateData("tbl_customers",$userdata,$whr);
			if($result > 0){
				$this->session->set_flashdata('success_msg',"Updated successfully!");
				redirect('customer');
			}else{
				$this->session->set_flashdata('error_msg',"Updation failed,please try again!");
			}
		}
		redirect('customer');
	}

	public function deletecustomer($id)
	{
		
	}
}
